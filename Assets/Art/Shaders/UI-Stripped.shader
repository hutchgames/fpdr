Shader "UI/Stripped"
{
	Properties
	{
		[PerRendererData] _MainTex ("Sprite Texture", 2D) = "white" {}
	}

	SubShader
	{
		Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent" "PreviewType"="Plane" "CanUseSpriteAtlas"="True"}

		Cull Off
		Lighting Off
		ZWrite Off
		ZTest [unity_GUIZTestMode]
		Blend SrcAlpha OneMinusSrcAlpha

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"
			
			struct appdata_t
			{
				float4	vertex		: POSITION;
				float4	color		: COLOR;
				float2	texcoord	: TEXCOORD0;
			};

			struct v2f
			{
				float4	vertex  	: SV_POSITION;
				fixed4	color  	 	: COLOR;
				half2	texcoord 	: TEXCOORD0;
			};

			v2f vert (appdata_t IN)
			{
				v2f OUT;
				OUT.vertex		= mul (UNITY_MATRIX_MVP, IN.vertex);
				OUT.texcoord	= IN.texcoord;
				OUT.color		= IN.color;
				return OUT;
			}

			sampler2D _MainTex;

			fixed4 frag (v2f IN) : SV_Target
			{
				half4 color = tex2D (_MainTex, IN.texcoord) * IN.color;
				return color;
			}
			ENDCG
		}
	}
}
