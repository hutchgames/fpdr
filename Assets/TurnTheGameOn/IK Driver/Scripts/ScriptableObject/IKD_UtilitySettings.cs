﻿using UnityEngine;
using System.Collections;

namespace TurnTheGameOn.IKDriver{
	public class IKD_UtilitySettings : ScriptableObject {
		
		#region Public Variables
		//Toggle mobile controls on or off.
		public bool useMobileController;
		#endregion
		
	}
}