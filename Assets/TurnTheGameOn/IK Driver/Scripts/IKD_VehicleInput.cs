﻿using UnityEngine;
using System.Collections;

namespace TurnTheGameOn.IKDriver{
	public class IKD_VehicleInput : EventSubscriber {
		

	public IKD_VehicleController m_vehicleController;

	public bool m_steeringActive = true;

	private bool m_isDriving = false;

	protected override void Awake()
	{
		base.Awake();

		if (!m_vehicleController)	
			m_vehicleController = GetComponent<IKD_VehicleController>();
	}

	private void FixedUpdate()
	{
		//h = Input.GetAxis(m_vehicleController.steeringAxis);
			//v = Input.GetAxis(m_vehicleController.throttleAxis);
			float h = TurnTheGameOn.IKDriver.IKD_CrossPlatformInputManager.GetAxis(m_vehicleController.steeringAxis);
			float v = TurnTheGameOn.IKDriver.IKD_CrossPlatformInputManager.GetAxis(m_vehicleController.throttleAxis);
			float emergencyBrake = 0;
			if (Input.GetKey (m_vehicleController.eBrakeKey) || Input.GetKey (m_vehicleController.eBrakeJoystick)) 
			{
				emergencyBrake = 1;
			}
				
			//m_vehicleController.Move(0f, 0f, 0f, 1f);
			// only use accelerator pre-race (revv during countdown)
			m_vehicleController.Move(m_steeringActive ? h : 0f, m_isDriving ? 1f  : v, 0f, 0f);
			//m_vehicleController.Move(h, v, v, emergencyBrake);
	}

	protected override void Subscribe ()
	{
		base.Subscribe ();

		SubscribeTo (GameEventCode.CountdownFinished, OnRaceStart);
		SubscribeTo (GameEventCode.Reset, OnReset);
	}

	private void OnRaceStart()
	{
		m_isDriving = true;
		m_vehicleController.ShiftUp();
		m_vehicleController.rbody.constraints = RigidbodyConstraints.None;
	}

	private void OnReset()
	{
		m_isDriving = false;
		m_vehicleController.Reset();
		m_vehicleController.rbody.constraints = RigidbodyConstraints.FreezePositionZ;
	}

	public void  SetSteeringActive( bool isActive)
	{
		m_steeringActive = isActive;
	}
}
}
