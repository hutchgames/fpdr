// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

Shader "Mobile/Diffuse Vertex Lit"
{

	Properties
	{
		_MainTex ("Base (RGB)", 2D) = "white" {}
	}
	
	CGINCLUDE		
	
	struct v2f_full
	{
		float4 pos : SV_POSITION;
		half4 uv : TEXCOORD0;
		fixed3 color : TEXCOORD2;
	};
		
	#include "UnityCG.cginc"		
	
	sampler2D _MainTex;	
							
	ENDCG
	
	SubShader
	{
		Tags { "RenderType" = "Opaque" "Reflection" = "RenderReflectionOpaque" }
		LOD 200
		
		Pass
		{
			Tags { "LightMode" = "ForwardBase" }
			
			CGPROGRAM
	
			float4 _MainTex_ST;
			fixed4 _LightColor0;	
			
			v2f_full vert (appdata_full v)
			{
				v2f_full o;
				o.pos = mul (UNITY_MATRIX_MVP, v.vertex);
				o.uv.xy = TRANSFORM_TEX(v.texcoord, _MainTex);
				
				half3 worldNormal = mul((fixed3x3)unity_ObjectToWorld, SCALED_NORMAL);
				half3 lightDirection = _WorldSpaceLightPos0.xyz;
				half3 diffuseReflection = _LightColor0 * max(0.0, dot(worldNormal, lightDirection));
				
				o.color = UNITY_LIGHTMODEL_AMBIENT.xyz;
				o.color += diffuseReflection * 2;
				
				return o; 
			}		
			
			fixed4 frag (v2f_full i) : COLOR0 
			{
				fixed4 tex = tex2D (_MainTex, i.uv.xy);
				tex.rgb *= i.color;
				return tex;
			}	
			
			#pragma vertex vert
			#pragma fragment frag
			#pragma fragmentoption ARB_precision_hint_fastest 
		
			ENDCG
		}
	}

Fallback "Mobile/VertexLit"
}
