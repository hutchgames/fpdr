﻿using UnityEngine;
using System.Collections;
using TurnTheGameOn.IKDriver;

public class NPC_VehicleInput : EventSubscriber 
{
		
	public IKD_VehicleController m_vehicleController;

	private bool m_isDriving = false;

	protected override void Awake()
	{
		base.Awake();

		if (!m_vehicleController)	
			m_vehicleController = GetComponent<IKD_VehicleController>();
	}

	private void FixedUpdate()
	{
		if (m_isDriving)
			m_vehicleController.Move(0f, 1f, 0f, 0f);
		else
			m_vehicleController.Move(0f, 0f, 1f, 1f);
	}

	protected override void Subscribe ()
	{
		base.Subscribe ();

		SubscribeTo (GameEventCode.CountdownFinished, OnRaceStart);
		SubscribeTo (GameEventCode.Reset, OnReset);
	}

	private void OnRaceStart()
	{
		m_isDriving = true;
		m_vehicleController.rbody.constraints = RigidbodyConstraints.None;
	}

	private void OnReset()
	{
		m_isDriving = false;
		m_vehicleController.Reset();
		m_vehicleController.rbody.constraints = RigidbodyConstraints.FreezePositionZ;
	}
	
}
