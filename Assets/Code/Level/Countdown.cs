﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Countdown : EventSubscriber 
{
	[SerializeField] Text m_countdownText;

	protected override void Subscribe ()
	{
		base.Subscribe ();

		SubscribeTo (GameEventCode.StartCountdown, OnStartCountdown);
	}

	public void OnStartCountdown()
	{
		StartCoroutine( DoCountdown() );
	}

	private IEnumerator DoCountdown()
	{
		for (int i=3; i>0; i--)
		{
			m_countdownText.text = i.ToString();
			yield return new WaitForSeconds(1);
		}
		m_countdownText.text = "GO";
		EventRouter.Global.RouteEvent (GameEventCode.CountdownFinished, null);

		yield return new WaitForSeconds(1);
		m_countdownText.text = "";
	}

}
