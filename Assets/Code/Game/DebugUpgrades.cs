﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugUpgrades : DebugSection 
{
	const string k_upgradeEconomyKey = "UpgradeEconomy";

	public enum UpgradeEconomy
	{
		None = -1,
		PipsPerAbility,
		TotalPips
	}

	protected override void Start ()
	{
		base.Start ();

		GameRules.m_upgradeEconomy = (UpgradeEconomy) PlayerPrefs.GetInt (k_upgradeEconomyKey, 0);
	}

	protected override void RenderDebugPanel ()
	{
		GUILayout.BeginVertical ();

		if (GUILayout.Button ("Reset Upgrades")) 
		{
			ResetUpgrades ();
		}

		if (GUILayout.Button ("Economy: " + GameRules.m_upgradeEconomy)) 
		{
			ToggleEconomy ();
		}

		GUILayout.EndVertical ();
	}

	private void ResetUpgrades ()
	{
		foreach (string playerType in System.Enum.GetNames (typeof (PlayerType)))
		{
			foreach (string upgradeType in System.Enum.GetNames (typeof (UpgradeType))) 
			{
				if (playerType != PlayerType.None.ToString () && upgradeType != UpgradeType.None.ToString ()) 
				{
					PlayerPrefs.SetInt (playerType + upgradeType, 0);
				}	
			}
		}
		PlayerPrefs.Save ();
		HelperMethods.Quit ();
	}

	private void ToggleEconomy ()
	{
		switch (GameRules.m_upgradeEconomy) 
		{
			case UpgradeEconomy.PipsPerAbility:
			{
				GameRules.m_upgradeEconomy = UpgradeEconomy.TotalPips;
				break;
			}
			case UpgradeEconomy.TotalPips:
			{
				GameRules.m_upgradeEconomy = UpgradeEconomy.None;
				break;
			}
			case UpgradeEconomy.None:
			default:
			{
				GameRules.m_upgradeEconomy = UpgradeEconomy.PipsPerAbility;
				break;
			}
		}
		PlayerPrefs.SetInt (k_upgradeEconomyKey, (int) GameRules.m_upgradeEconomy);
		PlayerPrefs.Save ();
	}
}
