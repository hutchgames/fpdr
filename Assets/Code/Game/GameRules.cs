﻿public static class GameRules
{
	public const float		k_laneWidth				= 5.0f;
	public const int		k_laneNumber			= 5;
	public const int		k_pipCount				= 22;
	public const float		k_damageMaxValue		= 136.5f;
	public const float		k_agilityMaxValue		= 2.72f;
	public const float		k_healthMaxValue		= 421.6f;
	public const float		k_fireRateMaxValue		= 8.1f;

	public static DebugUpgrades.UpgradeEconomy m_upgradeEconomy = DebugUpgrades.UpgradeEconomy.PipsPerAbility;

	public static int GetUpgradeCoinCost (int currentUpgradeLevel = 0, int totalUpgrades = 0)
	{
		switch (m_upgradeEconomy)
		{
			
			case DebugUpgrades.UpgradeEconomy.PipsPerAbility:
			{
				switch (currentUpgradeLevel) 
				{
					case 0: return 10;
					case 1: return 20;
					case 2: return 40;
					case 3: return 80;
					case 4: return 150;
					case 5: return 250;
					case 6: return 500;
					case 7: return 1000;
					case 8: return 1500;
					case 9: return 2000;
					default: return 2000;
				}
			}
			case DebugUpgrades.UpgradeEconomy.TotalPips:
			{
				switch (totalUpgrades) 
				{
					case 0: return 10;
					case 1: return 20;
					case 2: return 40;
					case 3: return 60;
					case 4: return 80;
					case 5: return 100;
					case 6: return 120;
					case 7: return 140;
					case 8: return 160;
					case 9: return 180;
					case 10: return 200;
					case 11: return 220;
					case 12: return 240;
					case 13: return 260;
					case 14: return 280;
					case 15: return 300;
					case 16: return 320;
					case 17: return 340;
					case 18: return 360;
					case 19: return 380;
					default: return 400;
				}
			}
			default:
			case DebugUpgrades.UpgradeEconomy.None:
			{
				return 0;
			}
		}
	}
}