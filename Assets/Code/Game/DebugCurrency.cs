﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugCurrency : DebugSection 
{
	protected override void RenderDebugPanel ()
	{
		GUILayout.BeginVertical ();

		if (GUILayout.Button ("+10 Coins")) 
		{
			EventRouter.Global.RouteEvent (GameEventCode.CoinCollected, 10);
		}

		if (GUILayout.Button ("+100 Coins")) 
		{
			EventRouter.Global.RouteEvent (GameEventCode.CoinCollected, 100);
		}

		if (GUILayout.Button ("+1000 Coins")) 
		{
			EventRouter.Global.RouteEvent (GameEventCode.CoinCollected, 1000);
		}

		if (GUILayout.Button ("-1000 Coins")) 
		{
			EventRouter.Global.RouteEvent (GameEventCode.CoinCollected, -1000);
		}

		if (GUILayout.Button ("-100 Coins")) 
		{
			EventRouter.Global.RouteEvent (GameEventCode.CoinCollected, -100);
		}

		if (GUILayout.Button ("-10 Coins")) 
		{
			EventRouter.Global.RouteEvent (GameEventCode.CoinCollected, -10);
		}

		GUILayout.EndVertical ();
	}
}
