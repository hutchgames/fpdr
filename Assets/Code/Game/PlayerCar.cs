﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof (HealthComponent))]
public class PlayerCar : MonoBehaviour 
{
	#region Constants

	const float					k_baseFireDelta					= 0.25f;
	const float					k_turnScale						= 0.1f;
	const float					k_swipeThreshold				= 4.0f;
	const float					k_swipeReduction				= 0.25f;
	const float					k_inputLerpRate					= 40.0f;
	const float					k_jumpDuration					= 1.5f;
	const float					k_invincibleDuration			= 1.0f;

	#endregion

	#region Serialized Data

	[SerializeField]
	private PlayerType			m_type							= PlayerType.None;

	[SerializeField]
	private Transform			m_bodyPivot						= null;

	[SerializeField]
	private float				m_width							= 2.5f;

	[SerializeField]
	private Transform[]			m_mainGuns						= null;

	[SerializeField]
	private Object				m_projectilePrefab				= null;

	[SerializeField]
	private float				m_maxStrafeSpeed				= 20f;
	[SerializeField]
	private float				m_strafeAcceleration			= 1f;

	[SerializeField]
	private float				m_buttonStrafeMultiplier		= 1f;

	[SerializeField]
	private float				m_laneSwitchDuration			= 0.5f;


	[SerializeField]
	private float				m_healthMax						= 100.0f;

	[SerializeField]
	[Range (0.05f, 2.0f)]
	private float				m_fireRate						= 1.0f;

	[SerializeField]
	[Range (0.05f, 2.0f)]
	private float				m_maneuverability				= 1.0f;

	[SerializeField]
	[Range (0.05f, 2.0f)]
	private float				m_ramming						= 1.0f;

	[SerializeField]
	private Transform			m_recoil						= null;

	[SerializeField]
	private float				m_damagePerHit					= 10.0f;

	[SerializeField]
	private Vector3				m_cameraOffset					= new Vector3 (0.0f, 4.0f, -98.0f);

	[SerializeField]
	private float				m_maxCameraRoll					= 20f;

	[SerializeField]
	private int[]				m_maxUpgrades					= new int[8] { 5, 5, 5, 5, 5, 5, 5, 5 };
		
	[SerializeField]
	private float[]				m_upgradeFudgeFactors			= new float[8] { 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f };

	#endregion

	#region Private Data

	private float				m_fireDelta						= k_baseFireDelta;
	private float				m_nextFire						= 0.0f;
	private VehicleState		m_state							= VehicleState.Alive;
	private Camera				m_camera						= null;
	private Vector3				m_lastWorldPos					= Vector3.zero;
	private float				m_turn							= 0.0f;
	private float				m_maxTurn						= 4.0f;
	private float				m_maxTilt						= 15.0f;
	private Vector2				m_tiltOffset					= new Vector2 (0.1f, 0.15f);
	private float				m_tilt							= 0.0f;
	private float				m_tiltRate						= 0.5f;
	private float				m_spring						= 4.0f;
	private HealthComponent		m_health						= null;
	private Vector3				m_targetPos						= Vector3.zero;
	private Vector3				m_filteredAccelerometerInput	= Vector3.zero;
	private Animator			m_animator						= null;
	private bool				m_isJumping						= false;
	private float				m_animStart						= 0.0f;
	private bool				m_hasRecoil						= false;
	private bool				m_isSetup						= false;
	private float				m_invincibleUntil				= 0.0f;
	private float				m_laneSwitchTimer				= 0.0f;

	private int[]				m_upgrades						= new int[8];

	private float				m_strafeSpeed					= 0.0f;

	#endregion

	#region Public Static

	public static bool			m_popupOnScreen					= false;

	#endregion

	#region Accessors

	public bool IsAlive 
	{ 
		get 
		{ 
			Setup ();
			return m_health.IsAlive;
		} 
	}

	public float Damage
	{
		get 
		{
			int		upgrades	= m_upgrades[(int) UpgradeType.Damage];
			float	fudge		= m_upgradeFudgeFactors[(int) UpgradeType.Damage];
			return m_damagePerHit + upgrades * GameRules.k_damageMaxValue / GameRules.k_pipCount / m_mainGuns.Length * fudge;
		}
	}

	public float Agility
	{
		get 
		{
			int		upgrades	= m_upgrades[(int) UpgradeType.Agility];
			float	fudge		= m_upgradeFudgeFactors[(int) UpgradeType.Agility];
			return m_maneuverability + upgrades * GameRules.k_agilityMaxValue / GameRules.k_pipCount * fudge;
		}
	}

	public float Health
	{
		get 
		{
			int		upgrades	= m_upgrades[(int) UpgradeType.Health];
			float	fudge		= m_upgradeFudgeFactors[(int) UpgradeType.Health];
			return m_healthMax + upgrades * GameRules.k_healthMaxValue / GameRules.k_pipCount * fudge;
		}
	}

	public float FireRate
	{
		get 
		{
			int		upgrades	= m_upgrades[(int) UpgradeType.FireRate];
			float	fudge		= m_upgradeFudgeFactors[(int) UpgradeType.FireRate];
			return m_fireRate + upgrades * GameRules.k_fireRateMaxValue / GameRules.k_pipCount / m_mainGuns.Length * fudge;
		}
	}

	#endregion

	#region Unity Methods

	void Start () 
	{
		Setup ();
	}

	void Update () 
	{
		if (Time.timeScale <= 0.0f) 
		{
			return;
		}

		UpdateState ();
	}

	void OnTriggerEnter (Collider other)
	{
		CheckCollision (other);
	}

	void OnTriggerStay (Collider other)
	{
		CheckCollision (other);
	}

	#endregion

	#region Private Methods



	private void Setup ()
	{
		if (!m_isSetup)
		{
			m_camera		= Camera.main;
			m_health		= GetComponent<HealthComponent> ();
			m_targetPos		= transform.position;
			m_animator		= GetComponentInChildren<Animator> ();
			m_hasRecoil		= m_recoil != null;

			if (Agility < 1.0f) 
			{
				m_maxTilt *= Agility;
			}

			LoadAllUpgrades ();
			RefreshAllStats ();
			m_isSetup = true;
		}
	}



	private void UpdateState ()
	{
		switch (m_state) 
		{
			case VehicleState.Alive:
			{	
				if (!m_health.IsAlive) 
				{
					SetState (VehicleState.Dead);
				}
				break;
			}
			case VehicleState.Invincible:
			{
				if (Time.time > m_invincibleUntil) 
				{
					m_health.Enabled = true;
					SetState (VehicleState.Alive);
				}
				break;
			}
		}
	}

	private void UpdateAnims ()
	{
		if (m_isJumping && Time.time > m_animStart + k_jumpDuration) 
		{
			Jump (false);
		}
		if (m_animator.enabled && !m_isJumping)
		{
			if (m_animator.GetCurrentAnimatorStateInfo (0).IsName ("PlayerDefault")) 
			{
				m_animator.enabled = false;
			}
		}
		if (m_hasRecoil) 
		{
			m_recoil.localPosition = Vector3.Lerp (m_recoil.localPosition, Vector3.zero, Time.deltaTime / (m_fireDelta * m_fireRate) * 0.5f);
		}
	}

	private void SetState (VehicleState state)
	{
		switch (state) 
		{
			case VehicleState.Dead:
			{
				EventRouter.Global.RouteEvent (GameEventCode.PlayerDied, transform.position.x);
				gameObject.SetActive (false);
				break;
			}
			case VehicleState.Invincible:
			{
				m_health.Enabled = false;
				break;
			}
		}
		m_state = state;
	}

	private void Fire ()
	{
		m_nextFire = Time.time + m_fireDelta / FireRate;
		for (int i = 0; i < m_mainGuns.Length; i++) 
		{
			GameObject obj = (GameObject) Instantiate (m_projectilePrefab, m_mainGuns[i].position, Quaternion.Euler (0.0f, 180.0f, 0.0f)) as GameObject;
			Projectile projectile = obj.GetComponent<Projectile> ();
			if (projectile != null) 
			{
				projectile.Damage = Damage;
			}
		}
		if (m_hasRecoil) 
		{
			m_recoil.localPosition = new Vector3 (0.0f, 0.0f, -3.0f);
		}
	}

	public void Jump (bool state = true)
	{
		m_health.SetActive (!state);
		m_isJumping = state;
		m_animStart = Time.time;
		m_animator.SetBool ("Jump", state);

		if (state) 
		{
			m_animator.enabled = true;
		}
	}

	private void CheckCollision (Collider other)
	{
		HealthComponent health = other.GetComponent<HealthComponent> ();
		if (health != null) 
		{
			float damage = health.GetCollisionDamage ();
			if (health.SwipeReduction && other.transform.position.z < transform.position.z + k_swipeThreshold) 
			{
				damage *= k_swipeReduction;
			}
			if (health.SwipeReduction && m_ramming > 1.0f) 
			{
				damage /= m_ramming;
			}
			m_health.Damage (damage);
			if (m_health.IsAlive) 
			{
				health.Damage (m_health.MaxHealth);
			}
		}
	}

	private void RefreshAllStats ()
	{
		float diff			= Health - m_health.MaxHealth;
		m_health.MaxHealth	= Health;
		m_health.Damage (-diff);
	}

	private void LoadAllUpgrades ()
	{
		LoadUpgrade (UpgradeType.Damage);
		LoadUpgrade (UpgradeType.Agility);
		LoadUpgrade (UpgradeType.Health);
		LoadUpgrade (UpgradeType.FireRate);
	}

	private void LoadUpgrade (UpgradeType upgradeType)
	{
		m_upgrades[(int)upgradeType] = PlayerPrefs.GetInt (m_type.ToString () + upgradeType.ToString (), 0);
	}

	private void SaveUpgrade (UpgradeType upgradeType)
	{
		PlayerPrefs.SetInt (m_type.ToString () + upgradeType.ToString (), m_upgrades[(int) upgradeType]);
		PlayerPrefs.Save ();
	}

	#endregion

	#region Public Methods

	public float GetStat( UpgradeType upgradeType, bool raw = false )
	{
		switch (upgradeType) 
		{
			case UpgradeType.Damage:
				return GetDamageStat(raw);
			case UpgradeType.Agility:
				return GetAgilityStat(raw);
			case UpgradeType.Health:
				return GetHealthStat(raw);
			case UpgradeType.FireRate:
				return GetFireRateStat(raw);
			default:
				return 0f;
		}
	}

	public float GetDamageStat (bool raw = false)
	{
		Setup ();
		float result = raw ? m_damagePerHit : Damage;
		return Mathf.Clamp01 (result / GameRules.k_damageMaxValue);
	}

	public float GetAgilityStat (bool raw = false)
	{
		Setup ();
		float result = raw ? m_maneuverability : Agility;
		return Mathf.Clamp01 (result / GameRules.k_agilityMaxValue);
	}

	public float GetHealthStat (bool raw = false)
	{
		Setup ();
		float result = raw ? m_healthMax : Health;
		return Mathf.Clamp01 (result / GameRules.k_healthMaxValue);
	}

	public float GetFireRateStat (bool raw = false)
	{
		Setup ();
		float result = raw ? m_fireRate : FireRate;
		return Mathf.Clamp01 (result * m_mainGuns.Length / GameRules.k_fireRateMaxValue);
	}

	public float GetHealthFraction ()
	{
		Setup ();
		return m_health.HeathFraction;
	}

	public void SetStartPosition (float x)
	{
		transform.position	= new Vector3 (x, transform.position.y, transform.position.z);
		m_targetPos			= transform.position;
		m_lastWorldPos		= transform.position;
	}

	public void SetInvincible (float duration = k_invincibleDuration)
	{
		m_invincibleUntil = Time.time + duration;
		SetState (VehicleState.Invincible);
	}

	public void Reset ()
	{
		Setup ();
		m_health.Reset ();
		m_state = VehicleState.Alive;
	}

	public void Upgrade (UpgradeType upgradeType, int amount = 1)
	{
		int index = (int) upgradeType;
		if (m_upgrades[index] + amount <= m_maxUpgrades[index]) 
		{
			m_upgrades[index] += amount;
		}
		RefreshAllStats ();
		SaveUpgrade (upgradeType);
	}

	public int GetUpgradeLevel (UpgradeType upgradeType)
	{
		return m_upgrades[(int) upgradeType];
	}

	public int GetMaxUpgradeLevel (UpgradeType upgradeType)
	{
		return m_maxUpgrades[(int) upgradeType];
	}

	public bool CanUpgrade (UpgradeType upgradeType)
	{
		int index = (int) upgradeType;
		return m_upgrades[index] < m_maxUpgrades[index];
	}

	public int GetTotalUpgrades ()
	{
		int count = 0;
		for (int i = 0; i < m_upgrades.Length; i++) 
		{
			count += m_upgrades[i];
		}
		return count;
	}

	#endregion
}
