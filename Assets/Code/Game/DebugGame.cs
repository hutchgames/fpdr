﻿	using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugGame : DebugSection 
{
	const string k_cameraModePref	= "CameraMode";
	const string k_controlMode		= "ControlMode";

	public enum CameraMode
	{
		None = -1,
		TopDown,
		Tilted,
		FirstPerson
	}

	public enum ControlMode
	{
		None = -1,
		Touch,
		Tilt,
		LaneTilt
	}

	public static CameraMode	m_cameraMode	= CameraMode.TopDown;
	public static ControlMode	m_controlMode	= ControlMode.Touch;

	protected override void Start ()
	{
		base.Start ();

		m_cameraMode						= (CameraMode) PlayerPrefs.GetInt (k_cameraModePref, 0);
		m_controlMode						= (ControlMode) PlayerPrefs.GetInt (k_controlMode, 0);

		SetCameraMode ();
	}

	void Update ()
	{
		if (Input.GetKeyDown (KeyCode.T)) 
		{
			ToggleCameraMode ();
		}

		if (Input.GetKeyDown(KeyCode.R))
		{
			EventRouter.Global.RouteEvent(GameEventCode.Reset);
		}
	}

	protected override void RenderDebugPanel ()
	{
		GUILayout.BeginVertical ();

		if (GUILayout.Button ("Reset Game")) 
		{
			//UnityEngine.SceneManagement.SceneManager.LoadScene (0);
			EventRouter.Global.RouteEvent(GameEventCode.Reset);
		}

		if (GUILayout.Button ("Camera Mode: " + m_cameraMode)) 
		{
			ToggleCameraMode ();
		}

		if (GUILayout.Button ("Control Mode: " + m_controlMode)) 
		{
			ToggleControlMode ();
		}


		GUILayout.EndVertical ();
	}

	private void ToggleCameraMode ()
	{
		switch (m_cameraMode) 
		{
			case CameraMode.TopDown:
			{
				m_cameraMode = CameraMode.Tilted;
				break;
			}
			case CameraMode.Tilted:
			{
				m_cameraMode = CameraMode.TopDown;
				break;
			}
		}
		SetCameraMode ();
	}

	private void SwitchToLandscapeMode( bool isLandscape )
	{
		Screen.autorotateToLandscapeLeft = isLandscape;
		Screen.autorotateToLandscapeRight = isLandscape;
		Screen.autorotateToPortrait = !isLandscape;
		Screen.autorotateToPortraitUpsideDown = !isLandscape;
	}

	private void ToggleControlMode ()
	{
		switch (m_controlMode) 
		{
			case ControlMode.Touch:
			{
				m_controlMode	= ControlMode.Tilt;
				m_cameraMode	= CameraMode.FirstPerson;
				SwitchToLandscapeMode( true );
				break;
			}
			case ControlMode.Tilt:
			{
				m_controlMode	= ControlMode.LaneTilt;
				break;
			}
			case ControlMode.LaneTilt:
			{
				m_controlMode	= ControlMode.Touch;
				m_cameraMode	= CameraMode.TopDown;
				SwitchToLandscapeMode( false );
				break;
			}
		}
		SetCameraMode ();
	}

	private void SetCameraMode ()
	{
		switch (m_cameraMode) 
		{
			case CameraMode.TopDown:
			{
				Camera.main.transform.localPosition = new Vector3 (0.0f, 42.0f, 14.89f); // -72.0f
				Camera.main.transform.localRotation = Quaternion.Euler (90.0f, 0.0f, 0.0f);
				break;
			}
			case CameraMode.Tilted:
			{
				Camera.main.transform.localPosition = new Vector3 (0.0f, 35.0f, -2.11f);
				Camera.main.transform.localRotation = Quaternion.Euler (72.0f, 0.0f, 0.0f);
				break;
			}
			case CameraMode.FirstPerson:
			{
				Camera.main.transform.localPosition = new Vector3 (0.0f, 4.0f, -11.11f);
				Camera.main.transform.localRotation = Quaternion.Euler (0.0f, 0.0f, 0.0f);
				break;
			}
		}
		PlayerPrefs.SetInt (k_controlMode, (int) m_controlMode);
		PlayerPrefs.SetInt (k_cameraModePref, (int) m_cameraMode);
		PlayerPrefs.Save ();
	}
}
