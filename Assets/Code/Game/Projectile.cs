﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof (Collider))]
public class Projectile : MonoBehaviour 
{
	[SerializeField]
	private float		m_speed					= 100.0f;

	[SerializeField]
	private float		m_damage				= 1.0f;

	[SerializeField]
	private float		m_lifetime				= 3.0f;

	[SerializeField]
	private Object		m_explosionPrefab		= null;

	[SerializeField]
	private bool		m_dropToGround			= false;

	[SerializeField]
	private float		m_explosionDuration		= 5.0f;

	private bool		m_enabled				= true;
	private float		m_dieAt					= 0.0f;
	private bool		m_hasExplosion			= false;

	public float Damage { get { return m_damage; } set { m_damage = value; } }

	void Awake ()
	{
		m_dieAt			= Time.time + m_lifetime;
		m_hasExplosion	= m_explosionPrefab != null;
	}

	void Update () 
	{
		Vector3 delta = transform.forward * m_speed * Time.deltaTime;

		if (m_dropToGround && transform.position.y > 2.0f) 
		{
			delta.y -= Mathf.Abs (m_speed * 0.25f) * Time.deltaTime;
		}

		transform.Translate (delta);
		if (Time.time > m_dieAt) 
		{
			Die (false);
		}
	}

	void OnTriggerEnter (Collider other)
	{
		if (m_enabled)
		{
			HealthComponent health = other.GetComponent<HealthComponent> ();
			if (health != null) 
			{
				float damage = m_hasExplosion ? 0.0f : m_damage;
				if (health.Damage (damage)) 
				{
					Die ();
				}
			}
		}
	}

	private void Die (bool hitSomething = true)
	{
		m_enabled = false;
		if (hitSomething && m_hasExplosion) 
		{
			GameObject obj = (GameObject) Instantiate (m_explosionPrefab, transform.position - transform.forward * 5.0f, Quaternion.identity) as GameObject;

			DamageZone zone = obj.GetComponent<DamageZone> ();
			if (zone != null) 
			{
				zone.Damage = m_damage;
			}

			Destroy (obj, m_explosionDuration);
		}
		Destroy (gameObject);
	}
}
