﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrafficLane : EventSubscriber
{
	[SerializeField] float		m_speed;

	[SerializeField] float		m_startDistance;
	[SerializeField] float 		m_spacing;

	[SerializeField] GameObject m_vehiclePrefab;

	[SerializeField] int		m_numVehicles;


	void Awake ()
	{
		base.Awake();	
	}


	protected override void Subscribe ()
	{
		base.Subscribe ();

		SubscribeTo (GameEventCode.Reset, OnReset);
		SubscribeTo (GameEventCode.StartCountdown, OnLevelStart);
	}

	void Update () 
	{
		transform.Translate (Vector3.forward * m_speed * Time.deltaTime, Space.World);
	}

	void OnLevelStart()
	{
		transform.position = transform.position.x * Vector3.right;
		Vector3 spawnPos = transform.position + m_startDistance * Vector3.forward;
		for ( int i = 0; i < m_numVehicles; i++ )
		{
			Instantiate( m_vehiclePrefab, spawnPos, Quaternion.identity, transform );
			spawnPos += Vector3.forward * m_spacing;
		}
	}

	void OnReset()
	{
		foreach ( Transform child in transform )
		{
			GameObject.Destroy( child.gameObject );
		}
	}


}
