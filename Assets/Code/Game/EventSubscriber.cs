﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventSubscriber : MonoBehaviour 
{
	protected List<RouterSubscription> m_eventSubscriptions = new List<RouterSubscription> ();

	protected virtual void Awake ()
	{
		Subscribe ();
	}

	protected virtual void OnDestroy ()
	{
		Unsubscribe ();
	}

	protected virtual void Subscribe () {}

	protected void SubscribeTo (System.Enum enumType, EventRouter.Handler0 callback)
	{
		m_eventSubscriptions.Add (EventRouter.Global.Subscribe (callback, enumType));
	}

	protected virtual void SubscribeTo (System.Enum enumType, EventRouter.Handler1 callback)
	{
		m_eventSubscriptions.Add (EventRouter.Global.Subscribe (callback, enumType));
	}

	protected virtual void SubscribeTo (System.Enum enumType, EventRouter.Handler2 callback)
	{
		m_eventSubscriptions.Add (EventRouter.Global.Subscribe (callback, enumType));
	}

	private void Unsubscribe ()
	{
		int numEventSubscriptions = m_eventSubscriptions.Count;
		for (int i = 0; i < numEventSubscriptions; i++) 
		{
			m_eventSubscriptions[i].Dispose ();
		}
		m_eventSubscriptions.Clear ();
	}
}
