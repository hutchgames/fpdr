﻿public enum GameEventCode
{
	None = -1,
	Win,
	Lose,
	CoinCollected,
	PickupCollected,
	RoadSpeedSet,
	GamePaused,
	PlayerDied,
	PlayerSelected,
	Reset,
	StartCountdown,
	CountdownFinished,
	RaceFinished,
}