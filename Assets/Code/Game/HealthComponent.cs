﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthComponent : MonoBehaviour 
{
	#region Constants

	const float						k_flashDuration				= 0.15f;
	const float						k_collisionDamageFactor		= 1.0f;
	const float						k_explosionDuration			= 5.0f;
	const float						k_pickupDuration			= 6.0f;

	#endregion

	#region Serialized Data

	[SerializeField]
	private float					m_maxHealth					= 100.0f;

	[SerializeField]
	private HealthComponent[]		m_nextModules				= null;

	[SerializeField]
	private GameObject[]			m_objectsToHide				= null;

	[SerializeField]
	private bool					m_enabled					= true;

	[SerializeField]
	private Material				m_damageFlashMaterial		= null;

	[SerializeField]
	private MeshRenderer[]			m_damageFlashMeshes			= null;

	[SerializeField]
	private Object					m_explosionPrefab			= null;

	[SerializeField]
	private Object					m_pickupPrefab				= null;

	[SerializeField]
	private int						m_materialIndex				= 0;

	[SerializeField]
	private bool					m_swipeReduction			= true;

	[SerializeField]
	private Transform				m_healthBar					= null;

	[SerializeField]
	private Collider				m_collider					= null;

	#endregion

	#region Private Data

	private float					m_health					= 1.0f;
	private Material[]				m_originalMaterials			= null;
	private float					m_unflashAt					= 0.0f;
	private bool					m_isFlashing				= false;
	private bool					m_hasHealthBar				= false;
	private float					m_healthBarX				= 1.0f;

	#endregion

	#region Debug Data

	public static bool				m_showHealthBars		= false;

	#endregion

	#region Accessors

	public bool IsAlive { get { return m_health > 0.0f; } }

	public float HeathFraction { get { return m_health / m_maxHealth; } }

	public float MaxHealth { get { return m_maxHealth; } set { m_maxHealth = value; } }

	public bool SwipeReduction { get { return m_swipeReduction; } }

	public bool Enabled 
	{ 
		get 
		{ 
			return m_enabled; 
		} 
		set 
		{ 
			m_enabled = value;
		} 
	}

	#endregion

	#region Unity Methods

	void Awake ()
	{
		m_health	= m_maxHealth;
	}

	void Start ()
	{
		m_hasHealthBar = m_healthBar != null;
		if (m_damageFlashMeshes != null) 
		{
			m_originalMaterials = new Material[m_damageFlashMeshes.Length];
			for (int i = 0; i < m_damageFlashMeshes.Length; i++) 
			{
				m_originalMaterials[i] = m_damageFlashMeshes[i].sharedMaterials[m_materialIndex];
			}
		}
		OnEnable ();
	}

	void OnEnable ()
	{
		if (m_hasHealthBar) 
		{
			m_healthBarX = m_healthBar.localScale.x;
			m_healthBar.parent.gameObject.SetActive (m_showHealthBars);
			if (m_showHealthBars) 
			{
				RefreshHealthBar ();
			}
		}
	}

	void Update ()
	{
		if (m_isFlashing && Time.time > m_unflashAt) 
		{
			EnableFlash (false);
		}
	}

	#endregion

	#region Public Methods

	public bool Damage (float damageAmount)
	{
		if (!Enabled) 
		{
			return false;
		}

		m_health -= damageAmount;

		if (m_health <= 0.0f) 
		{
			Die ();
		} 
		else if (m_health > m_maxHealth) 
		{
			m_health = m_maxHealth;
		}

		if (damageAmount > 0.0f && !m_isFlashing) 
		{
			EnableFlash (true);
		}

		if (m_hasHealthBar && m_showHealthBars) 
		{
			RefreshHealthBar ();
		}

		return true;
	}

	public void Heal (float healingAmount)
	{
		Damage (-healingAmount);
	}

	public float GetCollisionDamage ()
	{
		return m_health * k_collisionDamageFactor;
	}

	public void SetActive (bool state)
	{
		m_collider.enabled = state;
	}

	public void Reset ()
	{
		m_health	= m_maxHealth;
		m_enabled	= true;
		if (m_collider != null) 
		{
			m_collider.enabled = true;
		}
	}

	#endregion

	#region Private Methods

	private void Die ()
	{
		m_health			= 0.0f;
		m_collider.enabled	= false;
		m_enabled			= false;

		if (m_nextModules != null && m_nextModules.Length > 0) 
		{
			for (int i = 0; i < m_nextModules.Length; i++) 
			{
				m_nextModules[i].Enabled = true;
			}
		}

		if (m_objectsToHide != null && m_objectsToHide.Length > 0) 
		{
			for (int i = 0; i < m_objectsToHide.Length; i++) 
			{
				if (m_objectsToHide[i] != null)
				{
					m_objectsToHide[i].SetActive (false);
				}
			}
		}

		if (m_explosionPrefab != null) 
		{
			GameObject obj = (GameObject) Instantiate (m_explosionPrefab, transform.position + Vector3.up, Quaternion.identity);
			obj.transform.SetParent (transform.parent);
			Destroy (obj, k_explosionDuration);
		}

		if (m_pickupPrefab != null) 
		{
			Vector3 pos		= transform.position;
			pos.y			= 0.75f;
			GameObject obj	= (GameObject) Instantiate (m_pickupPrefab, pos, Quaternion.identity);
			Destroy (obj, k_pickupDuration);
		}
	}

	private void EnableFlash (bool state)
	{
		m_isFlashing	= state;
		m_unflashAt		= Time.time + k_flashDuration;
		if (m_damageFlashMeshes != null) 
		{
			for (int i = 0; i < m_damageFlashMeshes.Length; i++) 
			{
				Material[] materials = m_damageFlashMeshes [i].materials;
				materials[m_materialIndex] = state ? m_damageFlashMaterial : m_originalMaterials[i];
				m_damageFlashMeshes[i].materials = materials;
			}
		}
	}

	private void RefreshHealthBar ()
	{
		bool showBar = m_health < m_maxHealth && m_health > 0.0f;
		m_healthBar.parent.gameObject.SetActive (showBar);
		if (showBar) 
		{
			float x = m_healthBarX * Mathf.Clamp01 (m_health / m_maxHealth);
			m_healthBar.localScale = new Vector3 (x, m_healthBar.localScale.y, m_healthBar.localScale.z);
		}
	}

	#endregion
}
