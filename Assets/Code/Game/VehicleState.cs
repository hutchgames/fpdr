﻿public enum VehicleState
{
	None = -1,
	Alive,
	Dead,
	Invincible
}