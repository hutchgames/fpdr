﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof (Collider))]
public class DamageZone : MonoBehaviour 
{
	[SerializeField]
	private float		m_damageDeltaTime		= 0.25f;

	[SerializeField]
	private float		m_damage				= 100.0f;

	private float		m_nextEnable			= 0.0f;
	private bool		m_zoneEnabled			= true;

	private List<Collider> m_colliders			= new List<Collider> ();

	public float Damage { get { return m_damage; } set { m_damage = value; } }

	void OnEnable ()
	{
		m_nextEnable = Time.time;
	}

	void FixedUpdate () 
	{
		if (!m_zoneEnabled && Time.time > m_nextEnable) 
		{
			m_zoneEnabled = true;
		} 
		else if (m_zoneEnabled && Time.time >= m_nextEnable + Time.fixedDeltaTime * 2) 
		{
			m_zoneEnabled	= false;
			m_nextEnable	= Time.time + m_damageDeltaTime;
			m_colliders.Clear ();
		}
	}

	void OnTriggerEnter (Collider other)
	{
		CheckCollision (other);
	}

	void OnTriggerStay (Collider other)
	{
		CheckCollision (other);
	}

	private void CheckCollision (Collider other)
	{
		if (m_zoneEnabled)
		{
			if (!m_colliders.Contains (other))
			{
				m_colliders.Add (other);
				HealthComponent health = other.GetComponent<HealthComponent> ();
				if (health != null) 
				{
					health.Damage (m_damage);
				}
			}
		}
	}
}
