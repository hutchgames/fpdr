﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TurnTheGameOn.IKDriver;

public class FinishLine : EventSubscriber 
{
	[SerializeField] Text m_messageText;

	private int m_numFinished = 0;

	protected override void Subscribe ()
	{
		base.Subscribe ();

		SubscribeTo (GameEventCode.Reset, OnReset);
	}

	void OnTriggerEnter (Collider other)
	{
		IKD_VehicleController vehicle = other.GetComponentInParent<IKD_VehicleController> ();
		if (vehicle != null) 
		{
			m_numFinished++;

			if (vehicle.GetComponent<PlayerCar>() != null) 
			{
				StartCoroutine(ShowResults());
				EventRouter.Global.RouteEvent(GameEventCode.RaceFinished);
			}
		}
	}

	IEnumerator ShowResults()
	{
		bool playerWon = (m_numFinished == 1);
		//EventRouter.Global.RouteEvent (playerWon ? GameEventCode.Win : GameEventCode.Lose);
		m_messageText.text = "YOU";

		yield return new WaitForSeconds(0.3f);
		m_messageText.text = playerWon ? "YOU\nWON" : "YOU\nLOST";

		yield return new WaitForSeconds(2f);
		m_messageText.text = "";

		EventRouter.Global.RouteEvent(GameEventCode.Reset);
	}

	void OnReset()
	{
		EventRouter.Global.RouteEvent(UIScreenName.VehicleSelect);
		m_numFinished = 0;
	}
		


}
