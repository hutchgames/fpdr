﻿public enum UpgradeType
{
	None = -1,
	Tires,
	Engine,
	Body,
	Nitro,
	Damage,
	Agility,
	Health,
	FireRate,
	NumTypes

}