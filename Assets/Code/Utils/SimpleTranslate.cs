﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleTranslate : MonoBehaviour 
{
	[SerializeField]
	private Vector3 m_speed = Vector3.zero;

	void Update () 
	{
		transform.Translate (m_speed * Time.deltaTime, Space.World);
	}
}
