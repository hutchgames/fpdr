﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof (Renderer))]
public class UVScroll : MonoBehaviour 
{
	[SerializeField]
	private Vector2 m_speed = new Vector2 (0.0f, 1.0f);

	private Renderer m_renderer = null;

	void Start () 
	{
		m_renderer = GetComponent<Renderer> ();
	}

	void Update () 
	{
		float x = m_renderer.material.mainTextureOffset.x + m_speed.x * Time.deltaTime;
		float y = m_renderer.material.mainTextureOffset.y + m_speed.y * Time.deltaTime;

		if (x > 1.0f) 
		{
			x -= 1.0f;
		} 
		else if (x < -1.0f) 
		{
			x += 1.0f;
		}

		if (y > 1.0f) 
		{
			y -= 1.0f;
		} 
		else if (x < -1.0f) 
		{
			y += 1.0f;
		}

		m_renderer.material.mainTextureOffset = new Vector2 (x, y);
	}

	public void SetSpeed (Vector2 speed)
	{
		m_speed = speed;
	}
}
