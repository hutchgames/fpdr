﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof (Renderer))]
public class UVAnim : MonoBehaviour 
{
	private Renderer	m_renderer		= null;
	private int			m_nx			= 1;
	private int			m_ny			= 1;
	private int			m_x				= 0;
	private int			m_y				= 1;

	void Start () 
	{
		m_renderer	= GetComponent<Renderer> ();
		m_nx		= Mathf.RoundToInt (1.0f / m_renderer.material.mainTextureScale.x);
		m_ny		= Mathf.RoundToInt (1.0f / m_renderer.material.mainTextureScale.y);
	}

	void Update () 
	{
		m_x++;
		if (m_x > m_nx) 
		{
			m_x = 0;
			m_y++;
		}
		if (m_y > m_ny) 
		{
			m_y = 0;
		}
		m_renderer.material.mainTextureOffset = new Vector2 (1.0f / m_nx * m_x, 1.0f / m_ny * m_y);
	}
}
