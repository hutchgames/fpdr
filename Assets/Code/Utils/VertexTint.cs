﻿using UnityEngine;
using System.Collections;

public class VertexTint : MonoBehaviour 
{
	[SerializeField]
	private Color				m_startTint				= Color.white;

	private Color[]				m_colors				= null;
	private MeshFilter			m_meshFilter			= null;
	private Mesh				m_mesh					= null;

	void Start () 
	{
		m_meshFilter		= GetComponent<MeshFilter> ();
		m_mesh				= m_meshFilter.mesh;
		m_colors			= new Color[m_mesh.vertices.Length];
		Tint (m_startTint);
	}

	public void Tint (Color color)
	{
		for (int i = 0; i < m_colors.Length; i++) 
		{
			m_colors[i] = color;
		}
		m_mesh.colors = m_colors;
		m_meshFilter.mesh = m_mesh;
	}
}
