﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.Match;
using UnityEngine.Networking.Types;

[RequireComponent (typeof (NetworkManager))]
public class HutchNetworkManager : MonoBehaviour 
{
	public enum MatchmakingState
	{
		None = -1,
		LookingForMatch,
		CreatingMatch,
		JoiningMatch,
		InMatch,
		Error,
		Disconnected
	}

	#region Constants

	private const string			k_matchName						= "DefaultMatchName";
	private const uint				k_matchSize						= 2;
	private const bool				k_matchAdvertise				= true;
	private const string			k_matchPassword					= "";
	private const string			k_publicClientAddress			= "";
	private const string			k_privateClientAddress			= "";
	private const int				k_eloScore						= 1;
	private const int				k_requestDomain					= 0;
	private const int				k_startPageNumber				= 0;
	private const int				k_resultPageSize				= 10;
	private const bool				k_ignorePrivate					= false;
	private const int				k_listenPort					= 9000;
	private const float				k_matchJoinTimeout				= 7.5f;
	private const int				k_maxReconnectionAttempts		= 5;
	private const float				k_reconnectionDeltaTime			= 6.0f;

	#endregion

	#region Private Data

	private NetworkManager			m_networkManager				= null;
	private MatchmakingState		m_state							= MatchmakingState.None;
	private bool					m_isHost						= false;

	private float					m_joinedMatchAt					= 0.0f;
	private bool					m_autoReconnect					= false;
	private bool					m_reconnectNow					= false;
	private int						m_reconnectionAttempts			= 0;
	private float					m_nextReconnectionAttempt		= 0.0f;
	private int						m_domain						= k_requestDomain;

	static	HutchNetworkManager		m_instance						= null;
	static	NetworkID				m_networkId						= NetworkID.Invalid;
	static	NetworkID				m_lastMatchId					= NetworkID.Invalid;

	#endregion

	#region Unity Methods

	void Awake ()
	{
		if (m_instance == null) 
		{
			m_instance = this;
		}
	}

	void Start ()
	{
		m_networkManager = GetComponent<NetworkManager> ();
	}

	void OnEnable ()
	{
		if (m_instance == null) 
		{
			m_instance = this;
		}
	}

	void OnDestroy ()
	{
		m_instance = null;
	}

	void Update ()
	{
		switch (m_state)
		{
			case MatchmakingState.InMatch:
			{
				UpdateMatchState ();
				break;
			}
		}
		if (m_reconnectNow) 
		{
			UpdateDisconnectedState ();
		}
	}


	#endregion

	#region Public Methods

	public static void Join ()
	{
		if (m_instance != null) 
		{
			m_instance.FindMatch ();
		}
	}

	public static void Leave ()
	{
		if (m_instance != null) 
		{
			m_instance.LeaveMatch ();
		}
	}

	public static MatchmakingState GetState ()
	{
		if (m_instance != null) 
		{
			return m_instance.m_state;
		}
		return MatchmakingState.None;
	}

	public static void Disconnected ()
	{
		if (m_instance != null) 
		{
			m_instance.m_state = MatchmakingState.Disconnected;
			if (m_instance.m_autoReconnect) 
			{
				m_instance.m_reconnectNow = true;
			}
		}
	}

	public static void UnlistMatch ()
	{
		if (m_networkId != NetworkID.Invalid) 
		{
			NetworkManager.singleton.matchMaker.SetMatchAttributes (m_networkId, false, m_instance.m_domain, OnMatchUnlisted);
		}
	}

	public static void AutoReconnect (bool state)
	{
		if (m_instance != null) 
		{
			m_instance.m_autoReconnect = state;
		}
	}

	public static void SetDomain(int domain)
	{
		if (m_instance != null) 
		{
			m_instance.m_domain = domain;
		}
	}

	#endregion

	#region Private Methods

	private void FindMatch ()
	{
		m_state = MatchmakingState.LookingForMatch;
		m_networkManager.StartMatchMaker ();
		NetworkManager.singleton.matchMaker.ListMatches (k_startPageNumber, k_resultPageSize, k_matchName, k_ignorePrivate, k_eloScore, m_domain, OnMatchFound);
	}

	private void CreateMatch ()
	{
		m_state = MatchmakingState.CreatingMatch;
		m_networkManager.StartMatchMaker ();
		m_networkManager.matchMaker.CreateMatch (k_matchName, k_matchSize, k_matchAdvertise, k_matchPassword, k_publicClientAddress, k_privateClientAddress, k_eloScore, m_domain, OnMatchCreated);
	}

	private void JoinMatch (NetworkID networkId)
	{
		m_state = MatchmakingState.JoiningMatch;
		NetworkManager.singleton.matchMaker.JoinMatch (networkId, k_matchPassword, k_publicClientAddress, k_privateClientAddress, k_eloScore, m_domain, OnJoinedMatch);
	}

	private void LeaveMatch ()
	{
		if (m_state != MatchmakingState.Disconnected) 
		{
			m_lastMatchId		= NetworkID.Invalid;
			m_state				= MatchmakingState.None;
			m_autoReconnect		= false;
			m_reconnectNow		= false;
		}
		if (m_isHost) 
		{
			NetworkManager.singleton.StopHost ();
		} 
		else 
		{
			NetworkManager.singleton.StopClient ();
		}
	}

	private void EnteredMatch ()
	{
		m_state					= MatchmakingState.InMatch;
		m_joinedMatchAt			= Time.unscaledTime;
		m_reconnectionAttempts	= 0;
		m_reconnectNow			= false;
	}

	private void UpdateMatchState ()
	{
		if (Time.unscaledTime > m_joinedMatchAt + k_matchJoinTimeout && NetworkPlayer.NumberOfPlayers < 1) 
		{
			Disconnected ();
			Leave ();
			EventRouter.Global.RouteEvent (MultiplayerEventCode.TimeOut, null);
		}
	}

	private void UpdateDisconnectedState ()
	{
		if (m_instance.m_autoReconnect && m_lastMatchId != NetworkID.Invalid) 
		{
			if (Time.unscaledTime > m_nextReconnectionAttempt) 
			{
				m_nextReconnectionAttempt = Time.unscaledTime + k_reconnectionDeltaTime;
				m_reconnectionAttempts++;
				if (m_reconnectionAttempts > k_maxReconnectionAttempts) 
				{
					m_reconnectNow		= false;
					m_lastMatchId		= NetworkID.Invalid;
					EventRouter.Global.RouteEvent (MultiplayerEventCode.ExceededReconnectionAttempts, null);
				}
				else
				{
					if (m_networkManager.matchMaker == null) 
					{
						m_networkManager.StartMatchMaker ();
					}
					if (m_networkManager.matchMaker != null) 
					{
						JoinMatch (m_lastMatchId);
					}
				}
			}
		}
	}

	#endregion

	#region Networking Callbacks

	private void OnMatchFound (bool success, string extendedInfo, List<MatchInfoSnapshot> matches)
	{
		if (!success) 
		{
			m_state = MatchmakingState.Error;
			Debug.Log (extendedInfo);
		}
		else if (matches.Count == 0) 
		{
			CreateMatch ();
		} 
		else 
		{
			m_lastMatchId = matches[matches.Count - 1].networkId;
			JoinMatch (m_lastMatchId);
		}
	}

	private void OnRejoinMatch (bool success, string extendedInfo, List<MatchInfoSnapshot> matches)
	{
		if (!success) 
		{
			m_state = MatchmakingState.Error;
			Debug.Log (extendedInfo);
		}
		else 
		{
			for (int i = 0; i < matches.Count; i++) 
			{
				if (matches[i].networkId == m_lastMatchId) 
				{
					JoinMatch (m_lastMatchId);
				}
			}
		}
	}

	private void OnMatchCreated (bool success, string extendedInfo, MatchInfo matchInfo)
	{
		if (!success) 
		{
			m_state = MatchmakingState.Error;
			Debug.Log (extendedInfo);
		} 
		else 
		{
			m_isHost		= true;
			m_networkId		= matchInfo.networkId;
			NetworkServer.Listen (matchInfo, k_listenPort);
			NetworkManager.singleton.StartHost (matchInfo);
			EnteredMatch ();
		}
	}

	private void OnJoinedMatch (bool success, string extendedInfo, MatchInfo matchInfo)
	{
		if (!success) 
		{
			m_state = MatchmakingState.Error;
			Debug.Log (extendedInfo);
		} 
		else 
		{
			m_isHost = false;
			NetworkManager.singleton.StartClient (matchInfo);
			EnteredMatch ();
		}
	}
	
	static void OnMatchUnlisted (bool success, string extendedInfo) 
	{
		if (!success) 
		{
			Debug.Log (extendedInfo);
		} 
	}

	#endregion
}
