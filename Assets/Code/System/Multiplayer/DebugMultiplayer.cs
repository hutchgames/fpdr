﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugMultiplayer : DebugSection 
{
	protected override void RenderDebugPanel ()
	{
		GUILayout.BeginVertical ();

		HutchNetworkManager.MatchmakingState state = HutchNetworkManager.GetState ();

		GUILayout.Label ("State: " + state);

		GUI.enabled = state != HutchNetworkManager.MatchmakingState.InMatch;

		if (GUILayout.Button ("Join")) 
		{
			HutchNetworkManager.Join ();
		}

		GUI.enabled = state == HutchNetworkManager.MatchmakingState.InMatch;

		if (GUILayout.Button ("Leave")) 
		{
			HutchNetworkManager.Leave ();
		}

		GUILayout.EndVertical ();
	}
}
