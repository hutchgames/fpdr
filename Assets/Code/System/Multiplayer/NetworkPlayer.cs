﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class NetworkPlayer : NetworkBehaviour
{
	#region Constants

	const string					k_defaultPlayerName		= "Player";
	const float						k_heatbeatDelta			= 7.5f;
	const int						k_maxNameLength			= 12;

	#endregion

	#region Static Data

	static List<NetworkPlayer>		m_players				= new List<NetworkPlayer> ();
	static NetworkPlayer			m_localPlayer			= null;

	#endregion

	#region Private Data

	private string					m_playerName			= k_defaultPlayerName;
	private int						m_playerIndex			= -1;
	private bool					m_isLocalPlayer			= false;
	private float					m_nextHeatbeat			= 0.0f;

	#endregion

	#region Public Data

	private bool					m_debug					= true;

	#endregion

	#region Accessors

	public string Name 
	{ 
		get 
		{
			if (m_playerName == null) 
			{
				return "";
			}
			else if (m_playerName.Length <= k_maxNameLength) 
			{
				return m_playerName;
			}
			return m_playerName.Substring (0, k_maxNameLength);
		}
	}

	public int Index { get { return m_playerIndex; } }

	#endregion

	#region Static Accessors

	public static int NumberOfPlayers { get { return m_players.Count; } }

	public static NetworkPlayer LocalPlayer { get { return m_localPlayer; } }

	#endregion

	#region Static Methods

	public static NetworkPlayer GetPlayer (int index)
	{
		if (index < m_players.Count) 
		{
			return m_players[index];
		}
		return null;
	}

	#endregion

	#region Unity Methods

	void Start ()
	{
		m_isLocalPlayer = isLocalPlayer;
		OnStart ();
	}

	void Update () 
	{
		if (!isLocalPlayer) 
		{
			return;
		}
		if (isServer) 
		{
			UpdateServer ();
		}
		UpdateClient ();
	}

	void OnDestroy ()
	{
		if (m_players.Contains (this)) 
		{
			m_players.Remove (this);
		}
		RefreshPlayerIndicies ();
		if (m_isLocalPlayer) 
		{
			EventRouter.Global.RouteEvent (MultiplayerEventCode.PlayerLeft, "You");
			HutchNetworkManager.Disconnected ();
		} 
		else 
		{
			EventRouter.Global.RouteEvent (MultiplayerEventCode.PlayerLeft, m_playerName);
		}
	}

	#endregion

	#region Private Methods

	private void SetPlayerIndex (int index)
	{
		m_playerIndex = index;
	}

	private void RefreshPlayerIndicies ()
	{
		for (int i = 0; i < m_players.Count; i++)
		{
			m_players[i].SetPlayerIndex (i);
		}
	}

	protected virtual void OnStart ()
	{
		EventRouter.Global.RouteEvent (MultiplayerEventCode.PlayerJoined, m_playerIndex);
	}

	protected virtual void UpdateServer () {}

	protected virtual void UpdateClient () 
	{
		if (Time.unscaledTime > m_nextHeatbeat) 
		{
			m_nextHeatbeat = Time.unscaledTime + k_heatbeatDelta;
			NetworkPlayer player = NetworkPlayer.LocalPlayer;
			if (player != null) 
			{
				CmdHeartbeat ((byte) m_playerIndex);
			}
		}
	}

	#endregion

	#region Server Commands (Client to Server)

	[Command]
	protected virtual void CmdHeartbeat (byte playerIndex)
	{
		if (m_debug) { Debug.Log ("Cmd: Heartbeat: " + playerIndex); }
		RpcHeartbeat (playerIndex);
	}
		
	#endregion

	#region Remote Procedure Calls (Server to Clients)

	[ClientRpc]
	protected virtual void RpcHeartbeat (byte playerIndex)
	{
		if (m_debug) { Debug.Log ("Rpc: Heartbeat: " + playerIndex); }
	}

	#endregion

	#region Network Overrides

	public override void OnStartClient ()
	{
		base.OnStartClient ();
		m_players.Add (this);
		m_playerIndex = m_players.Count - 1;
	}

	public override void OnStartLocalPlayer ()
	{
		base.OnStartLocalPlayer ();
		m_localPlayer = this;
		EventRouter.Global.RouteEvent (MultiplayerEventCode.LocalPlayerCreated, m_playerIndex);
	}

	#endregion
}
