﻿public enum MultiplayerEventCode 
{
	None = -1,
	PlayerJoined,
	PlayerLeft,
	LocalPlayerCreated,
	RemoteStartRace,
	TimeOut,
	ExceededReconnectionAttempts
}
