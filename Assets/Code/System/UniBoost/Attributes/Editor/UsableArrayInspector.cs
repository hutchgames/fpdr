using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

#if !UNIBOOST_NO_NAMESPACES
namespace UniBoost
{
#endif

	[CustomPropertyDrawer(typeof(UsableArrayAttribute))]
	public class UsableArrayInspector : PropertyDrawer
	{
		private static HashSet<int> s_isArrayUnfolded = new HashSet<int>();

		private static readonly float s_addMenuWidth = 20;

		private int PropertyHashCode(SerializedProperty property)
		{
			return property.name.GetHashCode() ^ property.serializedObject.targetObject.GetHashCode();
		}

		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			label.text = string.Format("{0} ({1} items)", label.text, property.arraySize);

			position.height = base.GetPropertyHeight(property, label);
			position.width -= s_addMenuWidth;

			int hashCode = PropertyHashCode(property);

			bool unfolded = s_isArrayUnfolded.Contains(hashCode);
			bool newUnfolded = EditorGUI.Foldout(position, unfolded, label);

			// Update folded out status
			if (newUnfolded != unfolded)
			{
				if (newUnfolded)
				{
					s_isArrayUnfolded.Add(hashCode);
				}
				else
				{
					s_isArrayUnfolded.Remove(hashCode);
				}
			}

			// Add in a menu button for adding new items
			Rect menuRect = new Rect(position);
			menuRect.x += menuRect.width;
			menuRect.width = s_addMenuWidth;
			if (GUI.Button(menuRect, "+"))
			{
				property.InsertArrayElementAtIndex(property.arraySize);
			}

			position.width += s_addMenuWidth;

			Rect contentsRect;
			Rect labelRect;
	
			EditorGUI.indentLevel++;

			if (newUnfolded)
			{
				// Draw all the sub-elements
				int index = 0;
				foreach (SerializedProperty childProp in property)
				{
					position.y += position.height;
					position.height = EditorGUI.GetPropertyHeight(childProp);

					contentsRect = position;
					contentsRect.x += contentsRect.width;
					contentsRect.width = Mathf.Round(contentsRect.width * 0.47f);
					contentsRect.x -= contentsRect.width;

					EditorGUI.PropertyField(contentsRect, childProp,GUIContent.none, true);


					labelRect = position;
					labelRect.width *= 0.53f;
					labelRect.width -= 4 * s_addMenuWidth;

					EditorGUI.LabelField(labelRect, string.Format("[{0}]", index));

					menuRect.y = position.y;
					menuRect.x = labelRect.x + labelRect.width + s_addMenuWidth * 3;

					if (GUI.Button(menuRect, "+"))
					{
						property.InsertArrayElementAtIndex(index);
					}

					menuRect.x -= s_addMenuWidth;

					if (GUI.Button(menuRect, "-"))
					{
						property.DeleteArrayElementAtIndex(index);
					}

					menuRect.x -= s_addMenuWidth;
					GUI.enabled = index > 0;
					if (GUI.Button(menuRect, "\u2191"))
					{
						property.MoveArrayElement(index, index - 1);
					}

					menuRect.x -= s_addMenuWidth;

					GUI.enabled = index < (property.arraySize - 1);
					if (GUI.Button(menuRect, "\u2193"))
					{
						property.MoveArrayElement(index, index + 1);
					}

					GUI.enabled = true;


					index++;
				}
			}

			EditorGUI.indentLevel--;

		}

		public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
		{
			if (!property.isArray)
			{
				return base.GetPropertyHeight(property, label);
			}

			float height = base.GetPropertyHeight(property, label);

			if (s_isArrayUnfolded.Contains(PropertyHashCode(property)))
			{
				foreach (SerializedProperty childProp in property)
				{
					height += EditorGUI.GetPropertyHeight(childProp);
				}
			}

			return height;
		}
	}

#if !UNIBOOST_NO_NAMESPACES
}
#endif