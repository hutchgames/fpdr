using UnityEngine;
using System.Collections;

#if !UNIBOOST_NO_NAMESPACES
namespace UniBoost.Sys
{
#endif

	public class VisibleDependingOnAttribute : PropertyAttribute
	{
		public VisibleDependingOnAttribute(string _member, string[] _values)
		{
			member = _member;
			values = _values;
		}

		public VisibleDependingOnAttribute(string _member, string _value)
		{
			member = _member;
			values = new string[] { _value };
		}

		public VisibleDependingOnAttribute(string _member)
		{
			member = _member;
			values = new string[] { "true" };
		}

		public string member;
		public string[] values;
	}

#if !UNIBOOST_NO_NAMESPACES
}
#endif
