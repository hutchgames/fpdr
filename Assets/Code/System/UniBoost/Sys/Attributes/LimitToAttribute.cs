using UnityEngine;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;

#if !UNIBOOST_NO_NAMESPACES
namespace UniBoost.Sys
{
#endif

    public class LimitToAttribute : PropertyAttribute
    {

        public LimitToAttribute(System.Type attribute)
        {
			m_typeList = (from assembly in System.AppDomain.CurrentDomain.GetAssemblies()
			              from type in assembly.GetTypes()
			              from attr in type.GetCustomAttributes(attribute, true)
			              select type).ToArray();
        }

        public IList<System.Type> ValidTypes
        {
            get
            {
                return m_typeList;
            }
        }

        private System.Type[] m_typeList;
    }

#if !UNIBOOST_NO_NAMESPACES
}
#endif
