//          Copyright Timothy Aidley 2013
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//         http://opensource.org/licenses/BSL-1.0)

using System;

using UnityEngine;

#if !UNIBOOST_NO_NAMESPACES
namespace UniBoost.Sys
{
#endif

    /// <summary>
    /// Serializable enum. Stores the enum value as a string value for serialisation.
    /// </summary>
    [System.Serializable]
    public class SerializableEnum
    {
        public SerializableEnum()
        {
        }

        public SerializableEnum(System.Enum theEnum)
        {
            Enum = theEnum;
        }


        /// <summary>
        /// Gets the type represented by the enum
        /// </summary>
        public System.Type Type
        {
            get
            {
                return m_enumType.Type;
            }
        }

        /// <summary>
        /// Gets or sets the enum.
        /// </summary>
        public System.Enum Enum
        {
            get
            {
				bool refreshEnum = (m_enum == null);
				if ( !refreshEnum )
				{
					Type enumType = m_enum.GetType();
					refreshEnum = !enumType.Equals(m_enumType.Type);
				}
				
				if (refreshEnum)
					m_enum = (System.Enum)System.Enum.Parse(m_enumType.Type, m_enumStringName);

                return m_enum;
            }
            set
            {
                m_enumType.Type = value.GetType();
                m_enum = value;
                m_enumStringName = value.ToString();
            }
        }

        public static implicit operator System.Enum(SerializableEnum theEnum)
        {
            return theEnum.Enum;
        }

        public static implicit operator SerializableEnum(System.Enum theEnum)
        {
            return new SerializableEnum(theEnum);
        }

		public bool HasValue
		{
			get
			{
				return !string.IsNullOrEmpty(m_enumStringName);
			}
		}

        [SerializeField]
        private string m_enumStringName;

        [SerializeField]
        private SerializableType m_enumType = new SerializableType();

        private System.Enum m_enum = null;
    }

#if !UNIBOOST_NO_NAMESPACES
}
#endif
