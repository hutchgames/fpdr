//          Copyright Timothy Aidley 2013
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//         http://opensource.org/licenses/BSL-1.0)

using UnityEngine;
using System.Collections;
using System;

#if !UNIBOOST_NO_NAMESPACES
namespace UniBoost.Sys
{
#endif

	/// <summary>
	/// A class representing a GUID that can be serialized.
	/// The actual GUID data is serialized as a string.
	/// Guids are structs, and therefore not nullable; you
	/// can interrogate the SerializedGuid to determine whether
	/// or not it has actually been initialized.
	/// The class can be implicitly cast to and from normal Guids.
	/// </summary>
	[System.Serializable]
	public class SerializableGuid
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="SerializableGuid"/> class.
		/// </summary>
		public SerializableGuid()
		{
			m_isInitialized = false;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="SerializableGuid"/> class.
		/// </summary>
		/// <param name="guid">GUID.</param>
		public SerializableGuid(Guid guid)
		{
			Guid = guid;
			m_isInitialized = true;
		}

		/// <summary>
		/// Gets or sets the GUID.
		/// </summary>
		/// <value>The GUID.</value>
		public Guid Guid
		{
			get
			{
				if (!m_isInitialized)
				{
					if (!string.IsNullOrEmpty(m_guidString))
					{
						m_guid = new Guid(m_guidString);
						m_isInitialized = true;
					}
				}
				return m_guid;
			}
			set
			{
				if (!m_isInitialized || !value.Equals(m_guid))
				{
					m_guid = value;
					m_guidString = m_guid.ToString();
					m_isInitialized = true;
				}
			}
		}

		/// <summary>
		/// Implicit casting operator from SerializableGuid to Guid.
		/// </summary>
		public static implicit operator Guid(SerializableGuid other)
		{
			//HutchCore.Assert(other != null);
			return other.Guid;
		}

		/// <summary>
		/// Implicit casting operator from Guid to SerializableGuid.
		/// </summary>
		public static implicit operator SerializableGuid(Guid other)
		{
			return new SerializableGuid(other);
		}

		/// <summary>
		/// Checks whether the Guid has been initialized.
		/// </summary>
		/// <value><c>true</c> if this instance is initialized; otherwise, <c>false</c>.</value>
		public bool IsInitialized
		{
			get 
			{
				return m_isInitialized;
			}
		}

		/// <summary>
		/// Returns a <see cref="System.String"/> that represents the current <see cref="SerializableGuid"/>.
		/// </summary>
		/// <returns>A <see cref="System.String"/> that represents the current <see cref="SerializableGuid"/>.</returns>
		public override string ToString()
		{	
			return Guid.ToString();
		}

		/// <summary>
		/// Determines whether the specified <see cref="System.Object"/> is equal to the current <see cref="SerializableGuid"/>.
		/// </summary>
		/// <param name="obj">The <see cref="System.Object"/> to compare with the current <see cref="SerializableGuid"/>.</param>
		/// <returns><c>true</c> if the specified <see cref="System.Object"/> is equal to the current <see cref="SerializableGuid"/>;
		/// otherwise, <c>false</c>.</returns>
		public override bool Equals(object obj)
		{
			return m_guid.Equals(obj);
		}

		/// <summary>
		/// Determines whether the specified <see cref="SerializableGuid"/> is equal to the current <see cref="SerializableGuid"/>.
		/// </summary>
		/// <param name="other">The <see cref="SerializableGuid"/> to compare with the current <see cref="SerializableGuid"/>.</param>
		/// <returns><c>true</c> if the specified <see cref="SerializableGuid"/> is equal to the current
		/// <see cref="SerializableGuid"/>; otherwise, <c>false</c>.</returns>
		public bool Equals(SerializableGuid other)
		{
			return m_guid.Equals(other.m_guid);
		}

		/// <summary>
		/// Serves as a hash function for a <see cref="SerializableGuid"/> object.
		/// </summary>
		/// <returns>A hash code for this instance that is suitable for use in hashing algorithms and data structures such as a hash table.</returns>
		public override int GetHashCode()
		{
			return m_guid.GetHashCode();
		}
		

		[SerializeField]
		private string m_guidString;

		[NonSerialized]
		private Guid m_guid;
		[NonSerialized]
		private bool m_isInitialized;

	}

#if !UNIBOOST_NO_NAMESPACES
}
#endif
