//          Copyright Timothy Aidley 2013
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//         http://opensource.org/licenses/BSL-1.0)

#if !UNIBOOST_NO_NAMESPACES
namespace UniBoost.Sys
{
#endif

	/// <summary>
	/// Statically allocated singleton template; works only with classes that have a default
	/// constructor.
	/// </summary>
	/// <example>
	/// StaticSingleton<MyClass>.Instance.MyClassFunction();
	/// </example>
	public class StaticSingleton<T> where T : new()
	{
		// C# guarantees that static class objects are constructed at first access.
		private static readonly T s_instance = new T();
		
		private StaticSingleton() {}
		
		public static T Instance
		{
			get
			{
				return s_instance;
			}
		}
	}

#if !UNIBOOST_NO_NAMESPACES
}
#endif

