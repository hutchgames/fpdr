//          Copyright Timothy Aidley 2013
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//         http://opensource.org/licenses/BSL-1.0)

using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Linq;

#if !UNIBOOST_NO_NAMESPACES
namespace UniBoost.Sys
{
#endif

[CustomPropertyDrawer(typeof(SerializableGuid))]
public class SerializableGuidDrawer : PropertyDrawer
{
	private static readonly float guidWidth = 50;
	private static readonly float newWidth = 40;

	public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
	{
		EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);
		Rect stringBox = position;
		stringBox.width -= guidWidth;
		stringBox.x += guidWidth;

		stringBox.width -= newWidth;

		Rect newBox = new Rect(stringBox.x + stringBox.width, stringBox.y, newWidth, stringBox.height);

		SerializedProperty stringProperty = property.FindPropertyRelative("m_guidString");

		string newValue = EditorGUI.TextField(stringBox, GUIContent.none, stringProperty.stringValue);
		
		if (newValue != stringProperty.stringValue)
		{
			stringProperty.stringValue = newValue;
		}

		if (GUI.Button(newBox, "New"))
		{
			stringProperty.stringValue = System.Guid.NewGuid().ToString();
		}
	}



}

#if !UNIBOOST_NO_NAMESPACES
}
#endif
