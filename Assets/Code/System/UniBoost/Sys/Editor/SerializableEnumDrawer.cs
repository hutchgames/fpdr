//          Copyright Timothy Aidley 2013
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//         http://opensource.org/licenses/BSL-1.0)

using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Linq;

#if !UNIBOOST_NO_NAMESPACES
namespace UniBoost.Sys
{
#endif

    [CustomPropertyDrawer(typeof(SerializableEnum))]
    public class SerializableEnumDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			SerializedProperty typeProperty = property.FindPropertyRelative("m_enumType.m_typeName");

			if (typeProperty.stringValue == "")
			{
				SelectEnumType(position, typeProperty, label);
			}
			else
			{
				SelectEnumValue(position, property, label);
			}

		}

		private void SelectEnumValue(Rect position, SerializedProperty property, GUIContent label)
		{
			Rect dropDownRect = new Rect(position);
			dropDownRect.width -= 50;

			Rect resetRect = new Rect(position);
			resetRect.x = resetRect.xMax - 50;
			resetRect.width = 50;

			SerializedProperty typeProperty = property.FindPropertyRelative("m_enumType.m_typeName");
			SerializedProperty enumNameProperty = property.FindPropertyRelative("m_enumStringName");

			if (GUI.Button(resetRect, "Reset"))
			{
				typeProperty.stringValue = "";
				return;
			}

			System.Type type = System.Type.GetType(typeProperty.stringValue);

			if (type == null)
			{
				// Must be in a different assembly
				foreach (var assembly in System.AppDomain.CurrentDomain.GetAssemblies())
				{
					type = assembly.GetType(typeProperty.stringValue);
					if (type != null)
					{
						break;
					}
				}
				if (type == null)
				{
					GUI.Label(dropDownRect, "Could not find type " + typeProperty.stringValue);
					return;
				}
			}

			string[] enumvalues = System.Enum.GetNames(type);

			GUIContent labelText = (label == GUIContent.none) ? GUIContent.none : new GUIContent(string.Format("{0} ({1})", label.text, type.Name));

			int index = System.Array.IndexOf(enumvalues, enumNameProperty.stringValue);
			int newIndex = EditorGUI.Popup(dropDownRect, labelText.text , index, enumvalues);
			if (index != newIndex)
			{
				enumNameProperty.stringValue = enumvalues[newIndex];
			}


		}

		private void SelectEnumType(Rect position, SerializedProperty typeProperty, GUIContent label)
		{
            if (attribute != null)
            {
                LimitToTypeList typeLimiter = attribute as LimitToTypeList;
                LimitToAttribute attrLimiter = attribute as LimitToAttribute;
                if (typeLimiter != null)
                {
					SerializableTypeDrawer.SelectFromTypeLimited(typeLimiter, position, typeProperty, label);
                }
                else if (attrLimiter != null)
                {
					SerializableTypeDrawer.SelectFromAttributeLimited(attrLimiter, position, typeProperty, label);
                }
                else
                {
					SerializableTypeDrawer.SelectAnyEnum(position, typeProperty, label);
                }
            }
            else
            {
				SerializableTypeDrawer.SelectAnyEnum(position, typeProperty, label);
            }
        }


    }

#if !UNIBOOST_NO_NAMESPACES
}
#endif
