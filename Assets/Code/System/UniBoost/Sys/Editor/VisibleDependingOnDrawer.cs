using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.Reflection;

#if !UNIBOOST_NO_NAMESPACES
namespace UniBoost.Sys
{
#endif

	[CustomPropertyDrawer(typeof(VisibleDependingOnAttribute), true)]
	public class VisibleDependingOnDrawer : PropertyDrawer
	{
		private bool IsVisible(SerializedProperty property)
		{
			VisibleDependingOnAttribute visAttr = (VisibleDependingOnAttribute)attribute;

			if (visAttr.values != null)
			{
				SerializedProperty valueProp = property.serializedObject.FindProperty(visAttr.member);

				string propValue = string.Empty;

				switch (valueProp.propertyType)
				{
				case SerializedPropertyType.Enum:
					propValue = valueProp.enumNames[valueProp.enumValueIndex];
					break;

				case SerializedPropertyType.Float:
					propValue = valueProp.floatValue.ToString();
					break;
				
				case SerializedPropertyType.Integer:
					propValue = valueProp.intValue.ToString();
					break;

				case SerializedPropertyType.Boolean:
					return valueProp.boolValue.ToString().ToLower() == visAttr.values[0].ToLower();		// Allow just one Boolean value, so evaluate and return here.
				}

				if (!string.IsNullOrEmpty(propValue))
				{
					int numVisAttrValues = visAttr.values.Length;

					for (int i = 0; i < numVisAttrValues; ++i)
					{
						if (propValue == visAttr.values[i])
							return true;
					}

					return false;
				}
			}

			return true;
		}

		void DrawDefaultProperty(Rect position, SerializedProperty property, GUIContent label, bool includeChildren)
		{
			EditorGUI.PropertyField(position, property, label, includeChildren);
		}

		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			if (IsVisible(property))
				DrawDefaultProperty(position, property, label, true);
		}

		public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
		{
			if (IsVisible(property))
			{
				return base.GetPropertyHeight(property, label);
			}
			else
			{
				return 0;
			}
		}
	}

#if !UNIBOOST_NO_NAMESPACES
}
#endif
