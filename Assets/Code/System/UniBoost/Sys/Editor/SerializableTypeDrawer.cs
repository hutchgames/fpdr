//          Copyright Timothy Aidley 2013
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//         http://opensource.org/licenses/BSL-1.0)

using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Linq;
using System.Reflection;

#if !UNIBOOST_NO_NAMESPACES
namespace UniBoost.Sys
{
#endif

    [CustomPropertyDrawer(typeof(SerializableType))]
    public class SerializableTypeDrawer : PropertyDrawer
    {

		public static void SelectFromTypeLimited(LimitToTypeList typeLimiter, Rect position, SerializedProperty property, GUIContent label)
		{
			string[] typeNames = (from type in typeLimiter.ValidTypes
			                      select type.FullName).ToArray();

			ChooseFromSet(position, property, label, typeNames);
		}

		public static void SelectFromAttributeLimited(LimitToAttribute attrLimiter, Rect position, SerializedProperty property, GUIContent label)
		{
			string[] typeNames = (from type in attrLimiter.ValidTypes
			                      select type.FullName).ToArray();

			ChooseFromSet(position, property, label, typeNames);
		}

		public static void SelectAnyEnum(Rect position, SerializedProperty property, GUIContent label)
		{
			string[] typeNames = (from assembly in System.AppDomain.CurrentDomain.GetAssemblies()
			                       from type in assembly.GetTypes()
			                       where type.IsEnum
			                      select type.FullName).ToArray();

			ChooseFromSet(position, property, label, typeNames);
		}

		private static void ChooseFromSet(Rect position, SerializedProperty property, GUIContent label, string[] typeNames)
		{
			int index = System.Array.IndexOf(typeNames, property.stringValue);
			int newIndex = EditorGUI.Popup(position, label.text, index, typeNames);
			if (index != newIndex)
			{
				property.stringValue = typeNames[newIndex];
			}
		}

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            SerializedProperty typeProperty = property.FindPropertyRelative("m_typeName");

			LimitToTypeList typeLimiter = fieldInfo.GetCustomAttributes(typeof(LimitToTypeList), true).FirstOrDefault() as LimitToTypeList;
			LimitToAttribute attrLimiter = fieldInfo.GetCustomAttributes(typeof(LimitToAttribute), true).FirstOrDefault() as LimitToAttribute;
	        if (typeLimiter != null)
	        {
					SelectFromTypeLimited(typeLimiter, position, typeProperty, label);
	        }
	        else if (attrLimiter != null)
	        {
					SelectFromAttributeLimited(attrLimiter, position, typeProperty, label);
	        }
	        else
	        {
					SelectAnyEnum(position, typeProperty, label);
	        }
	            
	        }

        //public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        //{
        //    return base.GetPropertyHeight(property, label);
        //}
    }

#if !UNIBOOST_NO_NAMESPACES
}
#endif

