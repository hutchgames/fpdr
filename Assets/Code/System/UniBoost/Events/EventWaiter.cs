using UnityEngine;
using System.Collections;

public class EventWaiter
{
	private System.Enum m_result;
	public System.Enum Resut
	{
		get
		{
			return m_result;
		}
	}

	private object m_parameter;
	public object Parameter
	{
		get
		{
			return m_parameter;
		}
	}

	private RouterSubscription m_subscription;

	~EventWaiter()
	{
		if (m_subscription != null)
		{
			m_subscription.Dispose();
		}
	}

	public IEnumerator WaitForType(System.Type type)
	{
		m_result = null;
		m_subscription = EventRouter.Global.Subscribe(React, type);

		while (m_result == null)
		{
			yield return null;
		}
		m_subscription.Dispose();
		m_subscription = null;
	}



	public IEnumerator WaitForType(EventRouter router, System.Type type)
	{
		m_result = null;
		m_subscription = router.Subscribe(React, type);

		while (m_result == null)
		{
			yield return null;
		}
		m_subscription.Dispose();
		m_subscription = null;
	}

	public IEnumerator WaitForValues(params System.Enum[] values)
	{
		m_result = null;
		m_subscription = EventRouter.Global.Subscribe(React, values);

		while (m_result == null)
		{
			yield return null;
		}
		m_subscription.Dispose();
		m_subscription = null;
	}

	public IEnumerator WaitForValues(EventRouter router, params System.Enum[] values)
	{
		m_result = null;
		m_subscription = router.Subscribe(React, values);

		while (m_result == null)
		{
			yield return null;
		}
		m_subscription.Dispose();
		m_subscription = null;
	}


	private void React(System.Enum value, object parameter)
	{
		m_result = value;
		m_parameter = value;
	}

}
