using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

#if !UNIBOOST_NO_NAMESPACES
namespace UniBoost.Events
{
#endif

	[CustomPropertyDrawer(typeof(TestEventInEditorAttribute))]
	public class TestEventInEditorInspector : PropertyDrawer
	{
		public static readonly float g_testButtonWidth = 40;

		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			position.width -= g_testButtonWidth;

			// This is an evil hack to get the proper property drawer for the item
			Dictionary<string, PropertyDrawer> s_dictionary = typeof(PropertyDrawer).GetField("s_PropertyDrawers", BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic).GetValue(null) as Dictionary<string, PropertyDrawer>;

			foreach (var entry in s_dictionary)
			{
				if (entry.Value == this)
				{
					s_dictionary[entry.Key] = null;
					EditorGUI.PropertyField(position, property, label, true);
					s_dictionary[entry.Key] = this;

					position.x += position.width;
					position.width = g_testButtonWidth;

					if (GUI.Button(position, "Test"))
					{

					}

					return;
				}
			}
		}
	}

#if !UNIBOOST_NO_NAMESPACES
}
#endif
