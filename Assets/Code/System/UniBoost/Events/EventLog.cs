//////////////////////////////////////////////////////////////////////////
/// @file	EventLog.cs
///
/// @author	Tim Aidley
///
/// @brief 
///
/// @note 	
//////////////////////////////////////////////////////////////////////////
using System.Text;

#if UNITY_EDITOR
using UnityEngine;						
using UnityEditor;						
using System;							
using System.Collections;				
using System.Collections.Generic;	
using System.Diagnostics;




public class EventLog : EditorWindow
{
	private static EventLog g_instance;

	private static readonly string s_clearOnStartKey = "EventLog:ClearOnStart";

	private bool m_clearOnStart = false;

	private bool m_wasPaused = false;

	[MenuItem("Hutch/Window/Event Log")]
	public static void CreateEventLogWindow()
	{
		g_instance = GetWindow<EventLog>(ObjectNames.NicifyVariableName("EventLog"));
	}

	void OnEnable()
	{
		g_instance = this;
		m_clearOnStart = (EditorPrefs.GetInt(s_clearOnStartKey, 0) == 1);
		EditorApplication.playmodeStateChanged += OnPlaymodeStateChanged;
		m_wasPaused = false;
	}

	void OnDestroy()
	{
		g_instance = null;
		EditorApplication.playmodeStateChanged -= OnPlaymodeStateChanged;
	}

	void OnPlaymodeStateChanged()
	{
		if (EditorApplication.isPaused)
		{
			m_events.Add("----- PAUSED -----");
			m_callStacks.Add("No event selected");
			m_wasPaused = true;
		}
		else if (EditorApplication.isPlayingOrWillChangePlaymode)
		{
			if (m_clearOnStart && !m_wasPaused)
			{
				m_events.Clear();
				m_scrollPos = Vector2.zero;
			}
			else
			{
				m_events.Add("----- PLAYING -----");
				m_wasPaused = false;
			}
			m_callStacks.Add("No event selected");
		}
	}

	public static bool IsWindowOpen
	{
		get
		{
			return g_instance != null;
		}
	}

	private List<string> m_events = new List<string>();
	private List<string> m_callStacks = new List<string>();
	private Vector2 m_scrollPos = Vector2.zero;
	private int m_selectedIndex = -1;
	private string m_marker = "";

	public static void AddEvent(object evtType, object parameter)
	{
		if (parameter == null)
		{
			parameter = "null";
		}
		if (IsWindowOpen)
		{
			g_instance.m_events.Add(string.Format("{0}({1})", evtType.ToString(), parameter.ToString()));

			StringBuilder builder = new StringBuilder();
			StackTrace trace = new StackTrace(true);

			foreach(StackFrame frame in trace.GetFrames())
			{
				builder.AppendFormat("{0}:{1} (at {2}:{3})\n", frame.GetMethod().DeclaringType.Name, frame.GetMethod().Name, frame.GetFileName(), frame.GetFileLineNumber());
			}
			g_instance.m_callStacks.Add(builder.ToString());

			g_instance.Repaint();
		}
	}

	private void OnGUI()
	{
		EditorGUILayout.BeginHorizontal();
		if (GUILayout.Button("Clear", EditorStyles.toolbarButton))
		{
			m_events.Clear();
			m_callStacks.Clear();
			m_scrollPos = Vector2.zero;
			m_selectedIndex = -1;
		}
		GUIStyle buttonStyle = new GUIStyle(EditorStyles.toolbarButton);
		if (m_clearOnStart)
		{
			buttonStyle.normal.background = GUI.skin.button.normal.background;
		}
		if (GUILayout.Button("Clear on Play", buttonStyle	))
		{
			m_clearOnStart = !m_clearOnStart;
			EditorPrefs.SetInt(s_clearOnStartKey, m_clearOnStart ? 1 : 0);
		}
		if (GUILayout.Button("Add Marker", EditorStyles.toolbarButton))
		{
			m_events.Add("----" + m_marker + "----");
			m_callStacks.Add("No event selected");
		}
		m_marker = EditorGUILayout.TextField(GUIContent.none, m_marker, EditorStyles.toolbarTextField, GUILayout.MinWidth(50)); 
		EditorGUILayout.EndHorizontal();


		m_scrollPos = EditorGUILayout.BeginScrollView(m_scrollPos);
		int index = 0;
		foreach(string entry in m_events)
		{
			GUIStyle style = EditorStyles.label;
			if (index == m_selectedIndex)
			{
				style = EditorStyles.boldLabel;
			}
			if (GUILayout.Button(entry, style))
			{
				m_selectedIndex = index;
			}
			++index;
		}
		EditorGUILayout.EndScrollView();

		GUILayout.Box ("", GUILayout.ExpandWidth (true), GUILayout.Height (2));

		if (m_selectedIndex >= 0)
		{
			GUILayout.Label(m_callStacks[m_selectedIndex]);
		}
		else
		{
			GUILayout.Label("No event selected");
		}

	}
	
	
}

#endif

