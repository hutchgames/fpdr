﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

#if !UNIBOOST_NO_NAMESPACES
namespace UniBoost.Events
{
#endif

	public class EventBuffer : MonoBehaviour
	{
		private class RoutedEvent
		{
			public object parameter;
		}

		private class RoutedStringEvent : RoutedEvent
		{
			public RoutedStringEvent(string _eventType, object _parameter)
			{
				eventType = _eventType;
				parameter = _parameter;
			}
			public string eventType;
		}

		private class RoutedEnumEvent : RoutedEvent
		{
			public RoutedEnumEvent(System.Enum _eventType, object _parameter)
			{
				eventType = _eventType;
				parameter = _parameter;
			}
			public System.Enum eventType;
		}

		private Queue<RoutedEvent> m_eventQueue = new Queue<RoutedEvent>();

		public bool routeOnUpdate = false;

		public bool routeOnLateUpdate = true;

		public void BufferEvent(string eventType, object parameter)
		{
			m_eventQueue.Enqueue(new RoutedStringEvent(eventType, parameter));
		}

		public void BufferEvent(System.Enum eventType, object parameter)
		{
			m_eventQueue.Enqueue(new RoutedEnumEvent(eventType, parameter));
		}

		private void SendEvents()
		{
			while(m_eventQueue.Count != 0)
			{
				RoutedEvent evt = m_eventQueue.Dequeue();
//				Debug.Log("Routing");
				RoutedStringEvent strEvt = evt as RoutedStringEvent;
				if (strEvt != null)
				{
					EventRouter.Global.RouteEvent(strEvt.eventType, strEvt.parameter);
				}
				RoutedEnumEvent enumEvt = evt as RoutedEnumEvent;
				if (enumEvt != null)
				{
					EventRouter.Global.RouteEvent(enumEvt.eventType, enumEvt.parameter);
				}
			}
		}

		private void Update()
		{
			if (routeOnUpdate)
			{
				SendEvents();
			}
		}

		private void LateUpdate()
		{
			if (routeOnLateUpdate)
			{
				SendEvents();
			}
		}

	}

#if !UNIBOOST_NO_NAMESPACES
}
#endif