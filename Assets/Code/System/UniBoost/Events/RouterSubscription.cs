/// <summary>
/// Provides a disposable subscription interface to EventRouter.
/// </summary>

using UnityEngine;
using System.Collections.Generic;
using System;

#if !UNIBOOST_NO_NAMESPACES
namespace UniBoost.Events
{
#endif

	/// <summary>
	/// Wraps the add / remove handler code of EventRouter into a subscription object
	/// </summary>
	public class RouterSubscription : System.IDisposable
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="UniBoost.Events.RouterSubscription"/> class.
		/// This variant adds a handler for all events.
		/// </summary>
		/// <param name="router">Router.</param>
		/// <param name="handler">Handler.</param>
		public RouterSubscription(EventRouter router, EventRouter.Handler0 handler)
		{
			router.AddHandlerForAll(handler);
			m_handler = handler;
			m_router = router;
			m_subscriptionType = SubscriptionType.All;
		}

		public RouterSubscription(EventRouter router, EventRouter.Handler1 handler)
		{
			router.AddHandlerForAll(handler);
			m_handler = handler;
			m_router = router;
			m_subscriptionType = SubscriptionType.All;
		}

		public RouterSubscription(EventRouter router, EventRouter.Handler2 handler)
		{
			router.AddHandlerForAll(handler);
			m_handler = handler;
			m_router = router;
			m_subscriptionType = SubscriptionType.All;
		}

		public RouterSubscription(EventRouter router, EventRouter.HandlerString1 handler)
		{
			router.AddHandlerForAll(handler);
			m_handler = handler;
			m_router = router;
			m_subscriptionType = SubscriptionType.All;
		}

		public RouterSubscription(EventRouter router, EventRouter.HandlerString2 handler)
		{
			router.AddHandlerForAll(handler);
			m_handler = handler;
			m_router = router;
			m_subscriptionType = SubscriptionType.All;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="UniBoost.Events.RouterSubscription"/> class.
		/// This variant adds a handler for events of a certain type.
		/// </summary>
		/// <param name="router">Router.</param>
		/// <param name="handler">Handler.</param>
		/// <param name="type">Type.</param>
		public RouterSubscription(EventRouter router, EventRouter.Handler0 handler, Type type)
		{
			router.AddHandlerForType (handler, type);
			m_handler = handler;
			m_router = router;
			m_type = type;
			m_subscriptionType = SubscriptionType.Type;
		}

		public RouterSubscription(EventRouter router, EventRouter.Handler1 handler, Type type)
		{
			router.AddHandlerForType (handler, type);
			m_handler = handler;
			m_router = router;
			m_type = type;
			m_subscriptionType = SubscriptionType.Type;
		}

		public RouterSubscription(EventRouter router, EventRouter.Handler2 handler, Type type)
		{
			router.AddHandlerForType (handler, type);
			m_handler = handler;
			m_router = router;
			m_type = type;
			m_subscriptionType = SubscriptionType.Type;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="UniBoost.Events.RouterSubscription"/> class.
		/// This variant adds a handler for events with particular enum values.
		/// </summary>
		/// <param name="router">Router.</param>
		/// <param name="handler">Handler.</param>
		/// <param name="values">Values.</param>
		public RouterSubscription(EventRouter router, EventRouter.Handler0 handler, params Enum[] values)
		{
			router.AddHandlerForValues (handler, values);
			m_handler = handler;
			m_router = router;
			m_values = values;
			m_subscriptionType = SubscriptionType.Values;
		}

		public RouterSubscription(EventRouter router, EventRouter.Handler1 handler, params Enum[] values)
		{
			router.AddHandlerForValues (handler, values);
			m_handler = handler;
			m_router = router;
			m_values = values;
			m_subscriptionType = SubscriptionType.Values;
		}

		public RouterSubscription(EventRouter router, EventRouter.Handler2 handler, params Enum[] values)
		{
			router.AddHandlerForValues (handler, values);
			m_handler = handler;
			m_router = router;
			m_values = values;
			m_subscriptionType = SubscriptionType.Values;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="UniBoost.Events.RouterSubscription"/> class.
		/// This variant adds a handler for events with particular string events.
		/// </summary>
		/// <param name="router">Router.</param>
		/// <param name="handler">Handler.</param>
		/// <param name="values">Values.</param>
		public RouterSubscription(EventRouter router, EventRouter.Handler0 handler, params string[] strings)
		{
			router.AddHandlerForStrings (handler, strings);
			m_handler = handler;
			m_router = router;
			m_strings = strings;
			m_subscriptionType = SubscriptionType.String;
		}
		
		public RouterSubscription(EventRouter router, EventRouter.HandlerString1 handler, params string[] strings)
		{
			router.AddHandlerForStrings (handler, strings);
			m_handler = handler;
			m_router = router;
			m_strings = strings;
			m_subscriptionType = SubscriptionType.String;
		}
		
		public RouterSubscription(EventRouter router, EventRouter.HandlerString2 handler, params string[] strings)
		{
			router.AddHandlerForStrings (handler, strings);
			m_handler = handler;
			m_router = router;
			m_strings = strings;
			m_subscriptionType = SubscriptionType.String;
		}



		/// <summary>
		/// Releases unmanaged resources and performs other cleanup operations before the
		/// <see cref="UniBoost.Events.RouterSubscription"/> is reclaimed by garbage collection.
		/// </summary>
		~RouterSubscription()
		{
			Dispose();
		}

		/// <summary>
		/// Releases all resource used by the <see cref="UniBoost.Events.RouterSubscription"/> object.
		/// </summary>
		/// <remarks>Call <see cref="Dispose"/> when you are finished using the <see cref="UniBoost.Events.RouterSubscription"/>. The
		/// <see cref="Dispose"/> method leaves the <see cref="UniBoost.Events.RouterSubscription"/> in an unusable state.
		/// After calling <see cref="Dispose"/>, you must release all references to the
		/// <see cref="UniBoost.Events.RouterSubscription"/> so the garbage collector can reclaim the memory that the
		/// <see cref="UniBoost.Events.RouterSubscription"/> was occupying.</remarks>
		public void Dispose()
		{
			if (m_router == null)
			{
				return;
			}

			switch(m_subscriptionType)
			{
				case SubscriptionType.All:
					m_router.RemoveHandlerForAll (m_handler);
					break;
				case SubscriptionType.Type:
					m_router.RemoveHandlerForType (m_handler, m_type);
					break;
				case SubscriptionType.Values:
					m_router.RemoveHandlerForValues (m_handler, m_values);
					break;
				case SubscriptionType.String:
					m_router.RemoveHandlerForStrings (m_handler, m_strings);
					break;
			}
			m_router = null;
		}


		/// <summary>
		/// The various ways it is possible to subscribe to game events
		/// </summary>
		private enum SubscriptionType
		{
			All,
			Type,
			Values,
			String
		}

		private SubscriptionType m_subscriptionType;	
		private EventRouter m_router = null;
		private Delegate m_handler = null;
		private Type m_type;
		private Enum[] m_values;
		private string[] m_strings;
	}

	/// <summary>
	/// Extension methods for EventRouter so that the user can subscribe directly from a router.
	/// </summary>
	/// <example>
	/// EventRouter.Global.Subscribe(typeof(MyGameEventType), MyGameEventTypeHandler);
	/// </example>
	public static class RouterSubscriptionEventRouterExtensions
	{
		/// <summary>
		/// Subscribe the specified router with a handler to respond to all game events.
		/// </summary>
		/// <param name="router">Router.</param>
		/// <param name="handler">Handler.</param>
		public static RouterSubscription Subscribe(this EventRouter router, EventRouter.Handler0 handler)
		{
			return new RouterSubscription(router, handler);
		}

		public static RouterSubscription Subscribe(this EventRouter router, EventRouter.Handler1 handler)
		{
			return new RouterSubscription(router, handler);
		}

		public static RouterSubscription Subscribe(this EventRouter router, EventRouter.Handler2 handler)
		{
			return new RouterSubscription(router, handler);
		}

		/// <summary>
		/// Subscribe the specified router with a handler for all events of the specified type.
		/// </summary>
		/// <param name="router">Router.</param>
		/// <param name="handler">Handler.</param>
		/// <param name="type">Type.</param>
		public static RouterSubscription Subscribe(this EventRouter router, EventRouter.Handler0 handler, Type type)
		{
			return new RouterSubscription(router, handler, type);
		}

		public static RouterSubscription Subscribe(this EventRouter router, EventRouter.Handler1 handler, Type type)
		{
			return new RouterSubscription(router, handler, type);
		}

		public static RouterSubscription Subscribe(this EventRouter router, EventRouter.Handler2 handler, Type type)
		{
			return new RouterSubscription(router, handler, type);
		}

		/// <summary>
		/// Subscribe the specified router with a handler for all the supplied events.
		/// </summary>
		/// <param name="router">Router.</param>
		/// <param name="handler">Handler.</param>
		/// <param name="values">Values.</param>
		public static RouterSubscription Subscribe(this EventRouter router, EventRouter.Handler0 handler, params Enum[] values)
		{
			return new RouterSubscription(router, handler, values);
		}

		public static RouterSubscription Subscribe(this EventRouter router, EventRouter.Handler1 handler, params Enum[] values)
		{
			return new RouterSubscription(router, handler, values);
		}

		public static RouterSubscription Subscribe(this EventRouter router, EventRouter.Handler2 handler, params Enum[] values)
		{
			return new RouterSubscription(router, handler, values);
		}

		/// <summary>
		/// Subscribe the specified router with a handler for all the supplied event strings.
		/// </summary>
		/// <param name="router">Router.</param>
		/// <param name="handler">Handler.</param>
		/// <param name="values">Values.</param>
		public static RouterSubscription Subscribe(this EventRouter router, EventRouter.Handler0 handler, params string[] strings)
		{
			return new RouterSubscription(router, handler, strings);
		}
		
		public static RouterSubscription Subscribe(this EventRouter router, EventRouter.HandlerString1 handler, params string[] strings)
		{
			return new RouterSubscription(router, handler, strings);
		}
		
		public static RouterSubscription Subscribe(this EventRouter router, EventRouter.HandlerString2 handler, params string[] strings)
		{
			return new RouterSubscription(router, handler, strings);
		}
	}

#if !UNIBOOST_NO_NAMESPACES
}
#endif
