using UnityEngine;
using System.Collections;

#if !UNIBOOST_NO_NAMESPACES
namespace UniBoost.Events
{
#endif

    public class GameEventEnumAttribute : System.Attribute
    {
    }
#if !UNIBOOST_NO_NAMESPACES
}
#endif

