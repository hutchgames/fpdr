//          Copyright Timothy Aidley 2013
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//         http://opensource.org/licenses/BSL-1.0)

using UnityEngine;
using System.Collections.Generic;
using System;
using System.Linq;

#if !UNIBOOST_NO_NAMESPACES
namespace UniBoost.Events
{
#endif
	
	/// <summary>
	/// A class for routing game events
	/// </summary>
    public class EventRouter
    {
		/// <summary>
		/// The global instance of an EventRouter.
		/// </summary>
        public static EventRouter Global = new EventRouter();

		/// <summary>
		/// handler type with no parameters.
		/// </summary>
		public delegate void Handler0();
		/// <summary>
		/// handler type for functions that accept the enum event type.
		/// </summary>
		public delegate void Handler1(System.Enum enumValue);
		/// <summary>
		/// handler tyoe for functions that accept both the event type and a parameter.
		/// </summary>
		public delegate void Handler2(System.Enum enumValue, object parameter);
		/// <summary>
		/// Handler type for functions that accept a string as the event type
		/// </summary>
		public delegate void HandlerString1(string eventName);
		/// <summary>
		/// Handler type for functions that accept a string as the event type and a parameter
		/// </summary>
		public delegate void HandlerString2(string eventName, object parameter);


		#region Event Handlers storage.
		
		/// <summary>
		/// Event Handlers that respond to whole types.
		/// </summary>
		private Dictionary<Type, List<Delegate>> m_typeHandlers = new  Dictionary<Type, List<Delegate>>();

		/// <summary>
		/// Event Handlers that respond to individual enum values.
		/// </summary>
		private Dictionary<Enum, List<Delegate>> m_valueHandlers = new  Dictionary<Enum, List<Delegate>>();

		/// <summary>
		/// Event Handlers that respond to string events
		/// </summary>
		private Dictionary<string, List<Delegate>> m_stringHandlers = new Dictionary<string, List<Delegate>>();
		
		/// <summary>
		/// Event Handlers that respond to all events.
		/// </summary>
		private List<Delegate> m_everythingHandler = new List<Delegate>();
		
		#endregion

		#region For handling event removal during event handling

		private int m_handlingDepth = 0;

		private class RemovalData
		{
			public Delegate handlerToRemove;
			public List<Delegate> delegateList;
		}

		private Queue<RemovalData> m_removalList = new Queue<RemovalData>();

		private void RemoveDelegate(List<Delegate> dlList, Delegate handler)
		{
			if (m_handlingDepth == 0)
			{
				dlList.Remove(handler);
			}
			else
			{
				RemovalData removal = new RemovalData();
				removal.handlerToRemove = handler;
				removal.delegateList = dlList;
				m_removalList.Enqueue(removal);
			}
		}

		private void EmptyRemovalList()
		{
			while (m_removalList.Count != 0)
			{
				RemovalData removal = m_removalList.Dequeue();
				removal.delegateList.Remove(removal.handlerToRemove);
			}
		}

		#endregion
		
		
		#region Add and Remove Event Handlers.
		
		/// <summary>
		/// Adds the handler that will respond to all events.
		/// </summary>
		/// <param name='handler'>
		/// Handler delegate.
		/// </param>
		public void AddHandlerForAll(Handler0 handler)
		{
			m_everythingHandler.Add ( handler);
		}

		public void AddHandlerForAll(Handler1 handler)
		{
			m_everythingHandler.Add ( handler);
		}

		public void AddHandlerForAll(Handler2 handler)
		{
			m_everythingHandler.Add ( handler);
		}

		public void AddHandlerForAll(HandlerString1 handler)
		{
			m_everythingHandler.Add ( handler);
		}

		public void AddHandlerForAll(HandlerString2 handler)
		{
			m_everythingHandler.Add ( handler);
		}
		
		/// <summary>
		/// Removes a handler that has previously been added to respond to all events.
		/// </summary>
		/// <param name='handler'>
		/// Handler delegate.
		/// </param>
		public void RemoveHandlerForAll(Handler0 handler)
		{
			RemoveDelegate(m_everythingHandler, handler);
		}

		public void RemoveHandlerForAll(Handler1 handler)
		{
			RemoveDelegate(m_everythingHandler, handler);
		}

		public void RemoveHandlerForAll(Handler2 handler)
		{
			RemoveDelegate(m_everythingHandler, handler);
		}

		public void RemoveHandlerForAll(HandlerString1 handler)
		{
			RemoveDelegate(m_everythingHandler, handler);
		}

		public void RemoveHandlerForAll(HandlerString2 handler)
		{
			m_everythingHandler.Remove(handler);
		}

		public void RemoveHandlerForAll(Delegate handler)
		{
			RemoveDelegate(m_everythingHandler, handler);
		}
		
		/// <summary>
		/// Adds an event handler that responds to all events of a particular type.
		/// </summary>
		/// <param name='handler'>
		/// Handler delegate.
		/// </param>
		/// <param name='type'>
		/// The type the handler will respond to.
		/// </param>
		public void AddHandlerForType(Handler0 handler, Type type)
		{
			_AddHandlerForType(handler, type);
		}

		public void AddHandlerForType(Handler1 handler, Type type)
		{
			_AddHandlerForType(handler, type);
		}

		public void AddHandlerForType(Handler2 handler, Type type)
		{
			_AddHandlerForType(handler, type);
		}


		private void _AddHandlerForType(Delegate handler, Type type)
		{
			List<Delegate> foundHandler;
			if (!m_typeHandlers.TryGetValue(type, out foundHandler))
			{
				foundHandler = new List<Delegate>();
				m_typeHandlers.Add(type, foundHandler);
			}
			m_typeHandlers[type].Add(handler);

		}

		public void RemoveHandlerForType(Handler0 handler, Type type)
		{
			_RemoveHandlerForType(handler, type);
		}
		
		public void RemoveHandlerForType(Handler1 handler, Type type)
		{
			_RemoveHandlerForType(handler, type);
		}
		
		public void RemoveHandlerForType(Handler2 handler, Type type)
		{
			_RemoveHandlerForType(handler, type);
		}

		public void RemoveHandlerForType(Delegate handler, Type type)
		{
			_RemoveHandlerForType(handler, type);
		}
		
		/// <summary>
		/// Removes a handler previously added to respond to av event of a particular type.
		/// </summary>
		/// <param name='handler'>
		/// Handler delegate.
		/// </param>
		/// <param name='type'>
		/// Type to respond to.
		/// </param>
		private void _RemoveHandlerForType(Delegate handler, Type type)
		{
			List<Delegate> foundHandler;
			if (m_typeHandlers.TryGetValue(type, out foundHandler))
			{
				RemoveDelegate(foundHandler, handler);
			}
		}
		
		
		/// <summary>
		/// Adds an event handler for particular enum values.
		/// </summary>
		/// <param name='handler'>
		/// Handler delegate.
		/// </param>
		/// <param name='values'>
		/// Values that the event handler will respond to.
		/// </param>
		public void AddHandlerForValues(Handler0 handler, params Enum[] values)
		{
			_AddHandlerForValues(handler, values);
		}

		public void AddHandlerForValues(Handler1 handler, params Enum[] values)
		{
			_AddHandlerForValues(handler, values);
		}

		public void AddHandlerForValues(Handler2 handler, params Enum[] values)
		{
			_AddHandlerForValues(handler, values);
		}

		private void _AddHandlerForValues(Delegate handler, params Enum[] values)
		{
			foreach(Enum value in values)
			{
				List<Delegate> foundHandler;
				if (!m_valueHandlers.TryGetValue(value, out foundHandler))
				{
					foundHandler = new List<Delegate>();
					m_valueHandlers[value] = foundHandler;
				}
				foundHandler.Add(handler);
			}
		}

		public void RemoveHandlerForValues(Handler0 handler, params Enum[] values)
		{
			_RemoveHandlerForValues(handler, values);
		}
		
		public void RemoveHandlerForValues(Handler1 handler, params Enum[] values)
		{
			_RemoveHandlerForValues(handler, values);
		}
		
		public void RemoveHandlerForValues(Handler2 handler, params Enum[] values)
		{
			_RemoveHandlerForValues(handler, values);
		}

		public void RemoveHandlerForValues(Delegate handler, params Enum[] values)
		{
			_RemoveHandlerForValues(handler, values);
		}
		
		/// <summary>
		/// Remove a handler previously added to respond to particular values.
		/// </summary>
		/// <param name='handler'>
		/// Handler delegate.
		/// </param>
		/// <param name='values'>
		/// Values that the event handlers were registered to.
		/// </param>
		private void _RemoveHandlerForValues(Delegate handler, params Enum[] values)
		{
			foreach(Enum value in values)
			{
				List<Delegate> foundHandler;
				if (m_valueHandlers.TryGetValue(value, out foundHandler))
				{
					RemoveDelegate(foundHandler, handler);
					if (foundHandler.Count == 0)
					{
						m_valueHandlers.Remove(value);
					}
				}
			}
		}

		public void AddHandlerForStrings(Handler0 handler, params string[] strings)
		{
			_AddHandlerForStrings(handler, strings);
		}

		public void AddHandlerForStrings(HandlerString1 handler, params string[] strings)
		{
			_AddHandlerForStrings(handler, strings);
		}

		public void AddHandlerForStrings(HandlerString2 handler, params string[] strings)
		{
			_AddHandlerForStrings(handler, strings);
		}

		public void AddHandlerForStrings(Delegate handler, params string[] strings)
		{
			_AddHandlerForStrings(handler, strings);
		}

		private void _AddHandlerForStrings(Delegate handler, params string[] strings)
		{
			foreach(string strValue in strings)
			{
				List<Delegate> foundHandler;
				if (!m_stringHandlers.TryGetValue(strValue, out foundHandler))
				{
					foundHandler = new List<Delegate>();
					m_stringHandlers[strValue] = foundHandler;
				}
				foundHandler.Add(handler);
			}
		}

		public void RemoveHandlerForStrings(Handler0 handler, params string[] strings)
		{
			_RemoveHandlerForStrings(handler, strings);
		}
		
		public void RemoveHandlerForStrings(HandlerString1 handler, params string[] strings)
		{
			_RemoveHandlerForStrings(handler, strings);
		}
		
		public void RemoveHandlerForStrings(HandlerString2 handler, params string[] strings)
		{
			_RemoveHandlerForStrings(handler, strings);
		}

		public void RemoveHandlerForStrings(Delegate handler, params string[] strings)
		{
			_RemoveHandlerForStrings(handler, strings);
		}

		private void _RemoveHandlerForStrings(Delegate handler, params string[] strings)
		{
			foreach(string strValue in strings)
			{
				List<Delegate> foundHandler;
				if (m_stringHandlers.TryGetValue(strValue, out foundHandler))
				{
					RemoveDelegate(foundHandler, handler);
				}
			}
		}
		
		#endregion

		#region Event Routing methods

		/// <summary>
		/// Routes an event, with no arguments.
		/// </summary>
		/// <param name="eventValue">Event value.</param>
		public void RouteEvent(Enum eventValue)
		{
			RouteEvent(eventValue, null);
		}

		/// <summary>
		/// Routes an event using the given eventArguments object, while also supplying a sender.
		/// </summary>
		/// <param name="sender">Sender.</param>
		/// <param name="eventArgs">Event arguments.</param>
		public void RouteEvent(Enum eventType, object parameter)
        {
//			Debug.Log(string.Format("Received event {0}.", eventType));

			List<Delegate> foundHandler;
			if (m_valueHandlers.TryGetValue(eventType, out foundHandler))
            {
				Callhandlers(foundHandler, eventType, parameter);
            }
			if (m_typeHandlers.TryGetValue(eventType.GetType(), out foundHandler))
            {
				Callhandlers(foundHandler, eventType, parameter);
            }
			if (m_everythingHandler != null)
			{
				Callhandlers(m_everythingHandler, eventType, parameter);
			}
#if UNITY_EDITOR || UNITY_EDITOR_OSX
			EventLog.AddEvent(eventType, parameter);
#endif
        }

		public void RouteEvent(string eventString)
		{
			RouteEvent(eventString, null);
		}

		public void RouteEvent(string eventString, object parameter)
		{
			List<Delegate> foundHandler;
			if (m_stringHandlers.TryGetValue(eventString, out foundHandler))
		 	{
				Callhandlers(foundHandler, eventString, parameter);
			}
			if (m_everythingHandler != null)
			{
				Callhandlers(m_everythingHandler, eventString, parameter);
			}

#if UNITY_EDITOR || UNITY_EDITOR_OSX
			EventLog.AddEvent(eventString, parameter);
#endif
		}

		/// <summary>
		/// Internal method used to call event handlers. Wraps each invocation in the invocation
		/// list in a try/catch, but sends errors to the console.
		/// </summary>
		/// <param name="handlers">Handlers.</param>
		/// <param name="eventArgs">Event arguments.</param>
		private void Callhandlers(List<Delegate> handlers, Enum eventType, object parameter)
		{
			if (handlers == null)
			{
				return;
			}
			m_handlingDepth++;

			// Convert the input handlers to an array to avoid referencing issues if we add
			// handlers from handlers already in the input list...
			Delegate[] handlersArray = handlers.ToArray();
			for ( int i = 0; i < handlersArray.Length; i++ )
			{
				try
				{
					Delegate handler = (handlersArray[i] as Delegate);
					Handler0 h0;
					Handler1 h1;
					Handler2 h2;

					if ((h0 = handler as Handler0) != null)
					{
						h0();
					}
					else if ((h1 = handler as Handler1) != null)
					{
						h1(eventType);
					}
					else if ((h2 = handler as Handler2) != null)
					{
						h2(eventType, parameter);
					}
				}
				catch (Exception ex)
				{
					Debug.LogException(ex);
				}
			}
			m_handlingDepth--;
			EmptyRemovalList();
		}

		private void Callhandlers(List<Delegate> handlers, string eventString, object parameter)
		{
			if (handlers == null)
			{
				return;
			}
			m_handlingDepth++;
			foreach (Delegate handler in handlers)
			{
				try
				{
					Handler0 h0;
					HandlerString1 h1;
					HandlerString2 h2;
					
					if ((h0 = handler as Handler0) != null)
					{
						h0();
					}
					else if ((h1 = handler as HandlerString1) != null)
					{
						h1(eventString);
					}
					else if ((h2 = handler as HandlerString2) != null)
					{
						h2(eventString, parameter);
					}
				}
				catch (Exception ex)
				{
					Debug.LogException(ex);
				}
			}
			m_handlingDepth--;
			EmptyRemovalList();
		}

		#endregion
	}
        


#if !UNIBOOST_NO_NAMESPACES
}
#endif
	