using UnityEngine;
using System.Collections;

#if !UNIBOOST_NO_NAMESPACES
namespace UniBoost.Events
{
#endif

	public class EventReactionAttribute : PropertyAttribute
	{
		public string typeMember;

		public EventReactionAttribute()
		{
			typeMember = null;
		}

		public EventReactionAttribute(string _typeMember)
		{
			typeMember = _typeMember;
		}

	}

#if !UNIBOOST_NO_NAMESPACES
}
#endif