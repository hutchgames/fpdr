using UnityEngine;
using System.Collections;

#if !UNIBOOST_NO_NAMESPACES
namespace UniBoost.Events
{
#endif

	public class TestEventInEditorAttribute : PropertyAttribute
	{
	}

#if !UNIBOOST_NO_NAMESPACES
}
#endif
