﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugGUI : MonoBehaviour 
{
	private float m_startTime = 0.0f;

	void OnEnable ()
	{
		m_startTime = Time.time;
	}

	void OnGUI ()
	{
		Rect rt = new Rect (Screen.width / 2.0f - Screen.width * 0.25f, 5.0f, Screen.width * 0.25f, Screen.width * 0.065f);
		int fontSize = GUI.skin.label.fontSize;

		GUI.skin.label.fontSize = Mathf.RoundToInt (Screen.width / 640.0f * 22.0f);

		GUI.color = Color.green;

		GUI.Label (rt, "Time: " + (Time.time - m_startTime).ToString ("N1"));

		GUI.color = Color.white;
		GUI.skin.label.fontSize = fontSize;
	}
}
