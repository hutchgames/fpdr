//////////////////////////////////////////////////////////////////////////
/// @file	DebugPanel.cs
///
/// @author	Tim Aidley
///
/// @brief	A class for easily displaying debug controls on-device
///
/// @note 	
//////////////////////////////////////////////////////////////////////////

/************************ EXTERNAL NAMESPACES ***************************/

using UnityEngine;																// Unity 			(ref http://docs.unity3d.com/Documentation/ScriptReference/index.html)

using System;																	// String / Math 	(ref http://msdn.microsoft.com/en-us/library/system.aspx)
using System.Collections;														// Queue 			(ref http://msdn.microsoft.com/en-us/library/system.collections.aspx)
using System.Collections.Generic;												// List<> 			(ref http://msdn.microsoft.com/en-us/library/system.collections.generic.aspx)
using System.Linq;

/************************ REQUIRED COMPONENTS ***************************/


/************************** THE SCRIPT CLASS ****************************/

//////////////////////////////////////////////////////////////////////////
/// @brief	DebugPanel class.
//////////////////////////////////////////////////////////////////////////
[AddComponentMenu("Hutch/Debug/Debug Panel")]
public class DebugPanel : HutchSingleton<DebugPanel>
{

	/****************************** CONSTANTS *******************************/

	public enum SortOrder
	{
		Priority,
		InitOrder,
		Title,
	}


	public static readonly string s_savedScreenWidthKey = "DebugPanelSavedScreenWidth";

	/***************************** SUB-CLASSES ******************************/

	/// <summary>
	/// Delegate used to render debug sections.
	/// </summary>
	public delegate void RenderDebugGUI();

	/// <summary>
	/// Internal POD used for storing the sections.
	/// </summary>
	private class DebugSection
	{
		public int priority;

		public string sName;

		public RenderDebugGUI callback;

		public bool bOpened;
	}

	/// <summary>
	/// Delegate used to render debug sections.
	/// </summary>
	public delegate void OnDebugVisiblityChanged(bool isDebugVisible);

	/***************************** GLOBAL DATA ******************************/
	/*************************** GLOBAL METHODS *****************************/
	/***************************** PUBLIC DATA ******************************/

	/// <summary>
	/// Event that gets triggered whenever the visibility of the debug panel is changed.
	/// </summary>
	public static event OnDebugVisiblityChanged VisibilityChanged;

	/************************** SERIALIZED FIELDS ***************************/

	[SerializeField]
	private bool m_shouldDebugButtonBeVisible = true;

	[SerializeField]
	private Vector2 m_buttonSize = new Vector2(200, 50);

	[SerializeField]
	private float m_xPosition = 0.5f;

	[SerializeField]
	private GUISkin m_debugSkin;

	[SerializeField]
	private float m_transitionSpeed = 3.0f;

	[SerializeField]
	private bool m_scaleDebugPanel = false;

	[SerializeField]
	//[VisibleDependingOnAttribute("m_scaleDebugPanel")]
	private int m_screenWidth = 1024;

	[SerializeField]
	//[VisibleDependingOnAttribute("m_scaleDebugPanel")]
	private bool m_onlyScaleForBiggerScreens = true;

	[SerializeField]
	private SortOrder m_sortOrder = SortOrder.Priority;

	/***************************** PRIVATE DATA *****************************/

	//private SortedList<int, DebugSection> m_debugSections = new SortedList<int, DebugSection>();
	private List<DebugSection> m_debugSections = new List<DebugSection>();

	private bool m_isVisible = false;

	private Vector2 m_scrollPos = Vector2.zero;

	/// <summary>
	/// This is used to provide the opening and closing animation.
	/// Values between 0 (not visible) and 1 (full size)
	/// </summary>
	private float m_transitionScale = 0.0f;

	/// <note>
	/// As the game may be paused when the transition happens, we need to use a non-scaled timestep.
	/// As Time.realtimeSinceStartup is a float, it loses accuracy over time. AudioSettings.dspTime
	/// is a double so loses accuracy MUCH more slowly.
	/// </note>
	private double m_oldDspTime = 0.0f;

	// STEVEJ: uGUI EventSystem, which we disable when the debug panel is open.
	private GameObject m_eventSystem = null;

	/// <note>
	/// Custom styles used to render sections.
	/// </note>
	private GUIStyle m_topOpenButtonStyle;
	private GUIStyle m_openButtonStyle;
	private GUIStyle m_closeButtonStyle;
	private GUIStyle m_backgroundStyle;
	private GUIStyle m_titleStyle;

	public static GUIStyle TopOpenButtonStyle { get { return Get.m_topOpenButtonStyle; } } 
	public static GUIStyle OpenButtonStyle { get { return Get.m_openButtonStyle; } } 
	public static GUIStyle CloseButtonStyle { get { return Get.m_closeButtonStyle; } } 
	public static GUIStyle BackgroundStyle { get { return Get.m_backgroundStyle; } } 
	public static GUIStyle TitleStyle { get { return Get.m_titleStyle; } } 

	/**************************** CLASS METHODS *****************************/

	void Awake()
	{
		// ST: This is semantically wrong. Need a better of setting the instance and calling DontDestroyOnLoad.
		Create();

#if !DEVELOPMENT
		// Disable DebugPanel in production builds
		enabled = false;
#endif // !DEVELOPMENT
	}


	/// <summary>
	/// Gets the custom styles used by the debug panel
	/// </summary>
	void Start()
	{
		if ( m_debugSkin != null )
		{
			m_topOpenButtonStyle = m_debugSkin.customStyles[0];
			m_openButtonStyle = m_debugSkin.customStyles[1];
			m_closeButtonStyle = m_debugSkin.customStyles[2];
			m_backgroundStyle = m_debugSkin.customStyles[3];
			m_titleStyle = m_debugSkin.customStyles[4];
		}

		LoadSavedScreenWidth();
	}

	/// <summary>
	/// Registers a debug panel section with the debug section.
	/// Sections are keyed by name, so names should be unique.
	/// The ordering of sections can be controlled by setting the priority.
	/// </summary>
	/// <param name="sName">Name of the section.</param>
	/// <param name="nPriority">Priority of the section (suggest 100 = default).</param>
	/// <param name="callback">Delegate to be called to render the debug section.</param>
	public static void RegisterSection(string sName, int nPriority, RenderDebugGUI callback)
	{
		DebugSection section = new DebugSection()
		{
			priority = nPriority,
			sName = sName,
			callback = callback,
			bOpened = false,
		};

		if (Exists)
		{
			Get.m_debugSections.Add(section);
			switch (Get.m_sortOrder)
			{
				case SortOrder.Priority:
					Get.m_debugSections = Get.m_debugSections.OrderBy(x => x.priority).ThenBy(x => x.sName).ToList();
					break;
				case SortOrder.Title:
					Get.m_debugSections = Get.m_debugSections.OrderBy(x => x.sName).ToList();
					break;
				case SortOrder.InitOrder:
					// Don't sort at all
				default:
					break;
			}

		}
	}

	/// <summary>
	/// Unregisters a previously registered debug section.
	/// </summary>
	/// <param name="sName">Section name.</param>
	public static void UnregisterSection(string sName)
	{
		if (Exists)
		{
			var matchingSection = Get.m_debugSections.Where(x => x.sName == sName).FirstOrDefault();
			if (matchingSection != null)
			{
				int index = Get.m_debugSections.IndexOf(matchingSection);
				Get.m_debugSections.RemoveAt(index);
			}
		}
	}

	//////////////////////////////////////////////////////////////////////////
	/// @brief	Closes the debug panel.
	//////////////////////////////////////////////////////////////////////////
	public static void Close()
	{
		if (Get.m_isVisible)
		{
			Get.m_isVisible = false;

			if (Get.m_eventSystem != null)
			{
				Get.m_eventSystem.SetActive(true);
			}

			if (VisibilityChanged != null)
			{
				VisibilityChanged(Get.m_isVisible);
			}
		}
	}

	/// <summary>
	/// Displays the bulk of the debug panel, if it's visible.
	/// </summary>
	void DisplayDebugPanel(float screenWidth, float screenHeight)
	{
		if (m_transitionScale != 0)
		{
			Rect panelArea = new Rect(0, 0, screenWidth, Mathf.Max(screenHeight * m_transitionScale, 1));

			GUILayout.BeginArea(panelArea, m_backgroundStyle);

			GUILayout.Space(m_buttonSize.y * 1.25f);

			m_scrollPos = GUILayout.BeginScrollView(m_scrollPos);

			foreach (DebugSection section in m_debugSections)
			{
				if (section.bOpened)
				{
					GUILayout.BeginVertical(m_closeButtonStyle, GUILayout.ExpandWidth(true));

					if (GUILayout.Button(section.sName, m_titleStyle))
					{
						section.bOpened = false;
					}

					try
					{
						section.callback();
					}
					catch (Exception ex)
					{
						GUILayout.Label("Error in the code! See console!");
						GUILayout.Label(ex.Message);
						UnityEngine.Debug.LogException(ex);
					}

					GUILayout.EndVertical();
				}
				else
				{
					if (GUILayout.Button(section.sName, m_openButtonStyle, GUILayout.ExpandWidth(false)))
					{
						section.bOpened = true;
					}
				}
			}


			GUILayout.EndScrollView();
			GUILayout.EndArea();
		}
	}

	/// <summary>
	/// Displays the debug items.
	/// Will pause time when this happens.
	/// </summary>
	void OnGUI()
	{
		// Scale the whole gui if needed, so it doesn't get too diddy
		float ratio = Screen.width / (float)m_screenWidth;
		bool scale = m_scaleDebugPanel && (!m_onlyScaleForBiggerScreens || (ratio > 1.0f));
		Matrix4x4 oldMatrix = GUI.matrix;

		float screenWidth = Screen.width;
		float screenHeight = Screen.height;

		// If we're scaling, adjust the GUI matrix to fit the screen.
		if (scale)
		{
			Matrix4x4 scaleMatrix = new Matrix4x4();
			scaleMatrix.SetTRS(Vector3.zero, Quaternion.identity, Vector3.one * ratio);
			GUI.matrix *= scaleMatrix;
			screenWidth = m_screenWidth;
			screenHeight = Screen.height / ratio;
		}

		GUI.skin = m_debugSkin;

		DisplayDebugPanel(screenWidth, screenHeight);

		Rect debugBox = new Rect((screenWidth * m_xPosition) - (m_buttonSize.x / 2), 0, m_buttonSize.x, m_buttonSize.y);

		GUIStyle openButtonStyle = m_shouldDebugButtonBeVisible ? m_topOpenButtonStyle : GUIStyle.none;
		string openButtonText = m_shouldDebugButtonBeVisible ? "Open Debug" : "";

		if (m_isVisible)
		{
			if (GUI.Button(debugBox, "Close Debug", m_closeButtonStyle))
			{
				m_isVisible = false;

				if (m_eventSystem != null)
				{
					m_eventSystem.SetActive(true);
				}

				if (VisibilityChanged != null)
				{
					VisibilityChanged(m_isVisible);
				}
			}

			Rect leftBox = new Rect(0, 0, m_buttonSize.x, m_buttonSize.y);
			Rect rightBox = new Rect(screenWidth - m_buttonSize.x, 0, m_buttonSize.x, m_buttonSize.y);

			if (GUI.Button(leftBox, "Chunkier"))
			{
				m_screenWidth = (m_screenWidth * 3) / 4;
				SaveSavedScreemWidth();
			}

			if (GUI.Button(rightBox, "Slimmer"))
			{
				m_screenWidth = (m_screenWidth * 4) / 3;
				SaveSavedScreemWidth();
			}
		}
		else
		{
			if ( openButtonStyle != null )
			{
				if (GUI.Button(debugBox, openButtonText, openButtonStyle))
				{
					m_isVisible = true;

					if (m_eventSystem == null)
					{
						m_eventSystem = GameObject.Find("EventSystem");		// STEVEJ: Assume there is only one uGUI EventSystem game object.
					}

					if (m_eventSystem != null)
					{
						m_eventSystem.SetActive(false);
					}

					if (VisibilityChanged != null)
					{
						VisibilityChanged(m_isVisible);
					}
				}
			}
		}

		if (Event.current.type == EventType.Repaint)
		{
			float direction = m_isVisible ? 1.0f : -1.0f;
			m_transitionScale = Mathf.Clamp01(m_transitionScale + (float)(AudioSettings.dspTime - m_oldDspTime) * m_transitionSpeed * direction);
			m_oldDspTime = AudioSettings.dspTime;
		}

		GUI.matrix = oldMatrix;

	}

	// Helper for changing integer fields
	public static bool DebugIntControls(string label, ref int value, int min = int.MinValue, int max = int.MaxValue, int increment = 1)
	{
		int oldValue = value;
		GUILayout.BeginHorizontal();

		GUILayout.Label(label);
		if (GUILayout.Button("  -  "))
		{
			value -= increment;
		}
		GUILayout.Label(value.ToString());
		if (GUILayout.Button("  +  "))
		{
			value += increment;
		}

		value = Mathf.Clamp(value, min, max);

		GUILayout.EndHorizontal();

		return oldValue != value;
	}

	public static bool IsOpen
	{
		get
		{
			if (!Exists)
				return false;
			return Get.m_isVisible;
		}
	}

	private void LoadSavedScreenWidth()
	{
#if DEVELOPMENT
		m_screenWidth = PlayerPrefs.GetInt (s_savedScreenWidthKey, m_screenWidth);
#endif
	}
	
	private void SaveSavedScreemWidth()
	{
#if DEVELOPMENT
		PlayerPrefs.SetInt (s_savedScreenWidthKey, m_screenWidth);
		PlayerPrefs.Save ();
#endif
	}
}
