//////////////////////////////////////////////////////////////////////////
/// @file	HutchSingleton.cs
///
/// @author	Sean Turner (ST)
///
/// @brief	Generic singleton base class.
///
/// @note 	Copyright 2013 Hutch Games Ltd. All rights reserved.
//////////////////////////////////////////////////////////////////////////

#define DESTROY_DISABLES_LAZYCREATE												// Use this option to protect singleton from being accidently re-created by Get after it has been destroyed.

#define CONSTRUCTOR_SETS_INSTANCEx												// This doesnt work properly as cant call DontDestroyOnLoad in constructor. Keeping for reference as may solve this in the future.

/************************ EXTERNAL NAMESPACES ***************************/

using UnityEngine;																// Unity 			(ref http://docs.unity3d.com/Documentation/ScriptReference/index.html)
using System;																	// String / Math 	(ref http://msdn.microsoft.com/en-us/library/system.aspx)

/************************ REQUIRED COMPONENTS ***************************/

/************************** THE SCRIPT CLASS ****************************/

//////////////////////////////////////////////////////////////////////////
/// @brief HutchSingleton of derived type T.
//////////////////////////////////////////////////////////////////////////
public class HutchSingleton<T> : MonoBehaviour where T : MonoBehaviour
{
	/****************************** CONSTANTS *******************************/
	
	/***************************** SUB-CLASSES ******************************/
	
	/***************************** GLOBAL DATA ******************************/	
	
	private static T				g_instance				= null;		//!< Singleton instance
	
	private static bool				g_bLazyCreate			= true;		//!< If true will create an instance in Get calls
	
	/*************************** GLOBAL METHODS *****************************/

	/// @brief	Enable/disable Lazy-Creation of the singleton in Get calls. eg. disable this if you want Get to return null until an instance is loaded.
	/// @note	Explicitly calling Create() isnt lazy creation and will succeed even if this is set to false.
	public static bool LazyCreate { get { return g_bLazyCreate; } set { g_bLazyCreate = value; } }

#if CONSTRUCTOR_SETS_INSTANCE
	//////////////////////////////////////////////////////////////////////////
	/// @brief	Constructor sets instance.
	//////////////////////////////////////////////////////////////////////////
	public HutchSingleton()
	{
		SetInstance();
	}
#endif // CONSTRUCTOR_SETS_INSTANCE

	//////////////////////////////////////////////////////////////////////////
	/// @brief	Create singleton if it doesn't already exist.
	//////////////////////////////////////////////////////////////////////////
	public static T Create()
	{
		// Activate lazy-create so that Get can create the instance, ONLY if no instance exists yet.
		if(g_instance == null)
			g_bLazyCreate = true;

		// ST: use reflection to call static Get on inherited class T, expensive but done rarely so we'll live with it. 
		System.Reflection.PropertyInfo piGet = typeof(T).GetProperty("Get");
		
		if(piGet != null)
		{
			return(piGet.GetValue(null, null) as T);
		}
		
		return(Get);
	}
	
	//////////////////////////////////////////////////////////////////////////
	/// @brief	Destroy singleton.
	//////////////////////////////////////////////////////////////////////////
	public static void Destroy()
	{
		if(g_instance != null)
		{
			Destroy(g_instance.gameObject);
			g_instance = null;
		}

#if DESTROY_DISABLES_LAZYCREATE
		g_bLazyCreate = false;	//!< After the instance has been Destroy()'d force it to be Create()'d to catch calls to Get recreating it accidentally.
#endif // DESTROY_DISABLES_LAZYCREATE
	}
	
	//////////////////////////////////////////////////////////////////////////
	/// @brief	Whether singleton instance exists. eg. Use in OnDestroy methods before calling Get.
	//////////////////////////////////////////////////////////////////////////
	public static bool Exists
	{
		get { return(g_instance != null ? true : false); }
	}
	
	//////////////////////////////////////////////////////////////////////////
	/// @brief	Get the singleton instance. Will create it if it doesnt exist.
	/// @note	Will first try to find it, then try to load a prefab object, then lastly create it in code.
	//////////////////////////////////////////////////////////////////////////
	public static T Get
	{
		get
		{
			if(g_instance == null)
			{
				// First see if a T script already exists so can be placed in a scene in the editor.
				g_instance = FindObjectOfType(typeof(T)) as T;

				// Was an instance found?
				if(g_instance != null)
				{
					// Turn off lazy create just in case the instance gets destroyed behind our back 
					g_bLazyCreate = false;
				}
				else
				// No instance found so lazy create it now?
				if( g_bLazyCreate )
				{
					// Don't allow any further lazy-creation from this point on to catch calls to Get during Awake creating another instance accidentally.
					// Awake can optionally call SetInstance if they need to call Get on this instance.
					g_bLazyCreate = false;

					// Assert that all HutchSingletons are created in the main thread as Unity isn't thread-safe.
					//HutchCore.Assert(HutchSystem.IsMainThread(), "Creating "+typeof (T)+" not in the main thread.");

					// Load a prefab object with name of parameter class T
					String sTypeName = typeof(T).ToString();
					GameObject oPrefab = null;
					
					// First try loading a platform specific prefab (try Debug+Platform first in Development build)
#if UNITY_IPHONE
#if DEVELOPMENT
					if((oPrefab == null))
				 	 	oPrefab = (GameObject)Resources.Load(sTypeName+"Debug"+"iOS", typeof( GameObject ) );
#endif // DEVELOPMENT
					if(oPrefab == null)
						oPrefab = (GameObject)Resources.Load(sTypeName+"iOS", typeof( GameObject ) );
#elif AMAZON
					if((oPrefab == null) && HutchCore.IsDevelopmentBuild)
						oPrefab = (GameObject)Resources.Load(sTypeName+"Debug"+"Amazon", typeof( GameObject ) );
					if(oPrefab == null)
						oPrefab = (GameObject)Resources.Load(sTypeName+"Amazon", typeof( GameObject ) );
#elif UNITY_ANDROID
#if DEVELOPMENT
					if((oPrefab == null))
						oPrefab = (GameObject)Resources.Load(sTypeName+"Debug"+"Android", typeof( GameObject ) );
#endif // DEVELOPMENT
					if(oPrefab == null)
						oPrefab = (GameObject)Resources.Load(sTypeName+"Android", typeof( GameObject ) );
#elif UNITY_METRO
					if((oPrefab == null) && HutchCore.IsDevelopmentBuild)
						oPrefab = (GameObject)Resources.Load(sTypeName+"Debug"+"WSA", typeof( GameObject ) );
					if(oPrefab == null)
						oPrefab = (GameObject)Resources.Load(sTypeName+"WSA", typeof( GameObject ) );
#elif UNITY_WP8
					if((oPrefab == null) && HutchCore.IsDevelopmentBuild)
						oPrefab = (GameObject)Resources.Load(sTypeName+"Debug"+"WP8", typeof( GameObject ) );
					if(oPrefab == null)
						oPrefab = (GameObject)Resources.Load(sTypeName+"WP8", typeof( GameObject ) );
#elif UNITY_TVOS
					if((oPrefab == null) && HutchCore.IsDevelopmentBuild)
						oPrefab = (GameObject)Resources.Load(sTypeName+"Debug"+"TvOS", typeof( GameObject ) );
					if(oPrefab == null)
						oPrefab = (GameObject)Resources.Load(sTypeName+"TvOS", typeof( GameObject ) );
#endif
					
					// If that fails try loading a generic prefab (try Debug first in Development build)
#if DEVELOPMENT
					if(oPrefab == null) //&& HutchCore.IsDevelopmentBuild)
						oPrefab = (GameObject)Resources.Load(sTypeName+"Debug", typeof( GameObject ) );
#endif
					if(oPrefab == null)
						oPrefab = (GameObject)Resources.Load(sTypeName, typeof( GameObject ) );

					if(oPrefab != null)
					{
						// Instantiate the prefab: this calls Awake on all its scripts right here.
						GameObject o = Instantiate(oPrefab, Vector3.zero, Quaternion.identity) as GameObject;
						g_instance = o.GetComponent<T>();
					}

					// If prefab didnt exist then create an object and script in code with its default setup as a last resort
					if(g_instance == null)
					{
						g_instance = new GameObject(sTypeName).AddComponent<T>();
					}
				}

				// Assert that if we didnt create an instance that we already have one.
				//HutchCore.Assert(g_instance!=null, "ERROR: "+typeof(T)+".Get called when lazy create is disabled. Was Get called from within its own Awake or was singleton Destroyed and not Created again?"); 

				// Name object with class name for ease of identification in the hierarchy
				if(g_instance != null)
				{
					//g_instance.gameObject.name = typeof(T).ToString();
					DontDestroyOnLoad(g_instance.gameObject);
					DontDestroyOnLoad(g_instance);
				}
			}

			// NOTE: if lazy-create is off this can return null which is the correct behaviour. Calling code should check for this in OnDestroy situations.
			return(g_instance);
		}
	}
	
	/***************************** PUBLIC DATA ******************************/
	
	/***************************** PRIVATE DATA *****************************/

	/**************************** CLASS METHODS *****************************/

	//////////////////////////////////////////////////////////////////////////
	/// @brief	Force setting the instance to this, if not set already, so other classes can call Get during its Awake.
	/// 		Call during the singleton's Awake function (before g_instance has been set) IFF
	/// 		you are confident the singleton has been setup enough to be used by other classes.
	/// @note	This is required only because Unity discourages constructors on Monobehaviour classes (where we could set the instance).
	/// 		See: http://docs.unity3d.com/352/Documentation/ScriptReference/index.Writing_Scripts_in_Csharp_26_Boo.html
	//////////////////////////////////////////////////////////////////////////
	public void SetInstance()
	{
		if(g_instance == null)
		{
			g_instance = (T)(object)this;				// C# doesnt like downcasting but we know with certainty that this is of type T here.

			// Name object with class name for ease of identification in the hierarchy
			g_instance.gameObject.name = typeof(T).ToString();
			DontDestroyOnLoad(g_instance.gameObject);
			DontDestroyOnLoad(g_instance);
		}
	}
}


