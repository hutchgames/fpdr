﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class DebugSection : MonoBehaviour 
{
	protected string m_sectionName = "";

	protected virtual void Start () 
	{
		m_sectionName = this.GetType ().ToString ().Replace ("Debug", "");
		DebugPanel.RegisterSection (m_sectionName, 90, RenderDebugPanel);
	}

	protected virtual void OnDestroy ()
	{
		if (DebugPanel.Exists) 
		{
			DebugPanel.UnregisterSection (m_sectionName);
		}
	}

	protected abstract void RenderDebugPanel ();
}
