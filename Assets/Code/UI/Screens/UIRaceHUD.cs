﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIRaceHUD : UIScreen 
{
	[SerializeField] Image		m_playerProgressFill		= null;
	[SerializeField] Image		m_opponentProgressFill		= null;

	[SerializeField] Transform	m_playerTransform			= null;
	[SerializeField] Transform	m_opponentTransform			= null;
	[SerializeField] Transform	m_finishTransform			= null;

	[SerializeField] Text		m_timerText					= null;

	private float				m_raceTime = 0f;
	private bool				m_isInRace = false;

	void Update ()
	{
		float finishDistance = m_finishTransform.position.z;
		m_playerProgressFill.fillAmount = m_playerTransform.position.z/finishDistance;
		m_opponentProgressFill.fillAmount = m_opponentTransform.position.z/finishDistance;

		if (m_isInRace)
		{
			m_raceTime += Time.deltaTime;
			m_timerText.text  = m_raceTime.ToString("F2");
		}
	}

	protected override void Open ()
	{
		base.Open ();
		m_isInRace = false;
		m_timerText.text = "";
	}

	protected override void Subscribe ()
	{
		base.Subscribe ();
		SubscribeTo (GameEventCode.RaceFinished, OnRaceFinished);
		SubscribeTo (GameEventCode.CountdownFinished, OnRaceStart);

	}

	public void OnPauseButton ()
	{
		Close ();
		EventRouter.Global.RouteEvent (GameEventCode.GamePaused);
	}

	void OnRaceStart()
	{
		m_isInRace = true;
		m_raceTime = 0f;
	}

	void OnRaceFinished()
	{
		m_isInRace = false;
	}

}
