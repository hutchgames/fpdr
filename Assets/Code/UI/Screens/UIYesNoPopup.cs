﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIYesNoPopup : UIScreen 
{
	[SerializeField]
	private Text m_titleText		= null;

	[SerializeField]
	private Text m_bodyText			= null;

	[SerializeField]
	private Text m_yesButtonText	= null;

	[SerializeField]
	private Text m_noButtonText		= null;

	private Action m_yesCallback	= null;
	private Action m_noCallback		= null;

	protected override void Subscribe ()
	{
		base.Subscribe ();
		SubscribeTo (UIEventCode.YesNoPopup, OnYesNoPopup);
	}

	private void OnYesNoPopup (System.Enum enumType, object parameter)
	{
		m_yesCallback				= null;
		m_noCallback				= null;
		m_yesButtonText.text		= "YES";
		m_noButtonText.text			= "NO";
		m_titleText.text			= "";
		m_bodyText.text				= "Are you sure?";

		if (parameter != null)
		{
			YesNoPayload payload = (YesNoPayload) parameter as YesNoPayload;
			if (payload != null) 
			{
				m_yesCallback		= payload.m_yesCallback;
				m_noCallback		= payload.m_noCallback;
				m_titleText.text	= payload.m_title;

				if (!string.IsNullOrEmpty (payload.m_body)) 
				{
					m_bodyText.text = payload.m_body;
				}
				if (!string.IsNullOrEmpty (payload.m_yesString)) 
				{
					m_yesButtonText.text = payload.m_yesString;
				}
				if (!string.IsNullOrEmpty (payload.m_noString)) 
				{
					m_noButtonText.text = payload.m_noString;
				}
			}
		}

		Open ();
	}

	public void OnYesButtonClicked ()
	{
		Close ();
		if (m_yesCallback != null) 
		{
			m_yesCallback ();
		}
	}

	public void OnNoButtonClicked ()
	{
		Close ();
		if (m_noCallback != null) 
		{
			m_noCallback ();
		}
	}
}
