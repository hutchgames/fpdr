﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIVehicleSelectPopup : UIScreen 
{
	const string k_selectedCarPref = "SelectedCar";

	[SerializeField]
	private GameObject[]	m_playerVehicles		= null;

	[SerializeField]
	private GameObject		m_levelSections			= null;

	[SerializeField]
	private Image[]			m_selectionGlows		= null;

	[SerializeField]
	private Image[]			m_statPips				= null;

	[SerializeField]
	private Image[]			m_maxUpgradePips		= null;

	[SerializeField]
	private Image[]			m_upgradePips			= null;

	[SerializeField]
	private Image[]			m_specialAbilities		= null;

	[SerializeField]
	private Button[]		m_upgradeButtons		= null;

	[SerializeField]
	private Button[]		m_vehicleButtons		= null;

	[SerializeField]
	private Image[]			m_vehicleImages			= null;

	[SerializeField]
	private Material		m_aliveMaterial			= null;

	[SerializeField]
	private Material		m_deadMaterial			= null;

	[SerializeField]
	private Button			m_playButton			= null;

	[SerializeField]
	private RectTransform	m_pipRegion				= null;

	private int				m_selected				= 0;
	private float			m_startX				= 0.0f;
	private bool[]			m_isAlive				= null;
	private Color			m_selectedAliveColor	= Color.white;
	private bool			m_invincibleOnPlay		= false;
	private bool			m_hasPlayed				= false;
	private int				m_pendingUpgrade		= -1;

	protected override void Start ()
	{
		base.Start ();
		Open();

		m_selectedAliveColor	= m_selectionGlows[0].color;
		m_selected				= PlayerPrefs.GetInt (k_selectedCarPref, 0);
		m_pipRegion.sizeDelta	= new Vector2 (530.0f * GameRules.k_pipCount / 22.0f, m_pipRegion.sizeDelta.y);
		m_pipRegion.localScale	= new Vector3 (22.0f / GameRules.k_pipCount, 1.0f, 1.0f);
	}

	protected override void Subscribe ()
	{
		base.Subscribe ();

		SubscribeTo (GameEventCode.GamePaused, OnGamePaused);
		SubscribeTo (GameEventCode.PlayerDied, OnPlayerDied);
	}

	protected override void Open ()
	{
		Time.timeScale		= 1.0f;
		bool anyAlive		= false;
		int livesLeft		= 0;
		int fallbackIndex	= -1;
		m_isAlive			= new bool[m_playerVehicles.Length];
		m_hasPlayed			= false;

		for (int i = 0; i < m_playerVehicles.Length; i++) 
		{
			if (m_playerVehicles[i].activeInHierarchy) 
			{
				m_startX = m_playerVehicles[i].transform.position.x;
			}
			PlayerCar player = m_playerVehicles [i].GetComponent<PlayerCar> ();
			bool isAlive = false;
			if (player != null) 
			{
				if (player.IsAlive) 
				{
					livesLeft++;
					isAlive = true;
					if (!anyAlive) 
					{
						fallbackIndex	= i;
						anyAlive		= true;
					}
				}
			}
			m_isAlive[i]										= isAlive;
			m_vehicleButtons[i].GetComponent<Image> ().color	= isAlive ? Color.white : new Color (0.75f, 0.75f, 0.75f, 1.0f);
			m_vehicleImages[i].material							= isAlive ? m_aliveMaterial : m_deadMaterial;
			m_selectionGlows[i].color							= isAlive ? m_selectedAliveColor : new Color (0.5f, 0.5f, 0.5f, 1.0f);
		}

		OnVehicleSelected (m_selected);

		if (!m_isAlive[m_selected] && fallbackIndex >= 0) 
		{
			OnVehicleSelected (fallbackIndex);
		}

		if (livesLeft <= 0) 
		{
			Close ();
			EventRouter.Global.RouteEvent (GameEventCode.Lose);
		} 
		else 
		{
			PlayerCar.m_popupOnScreen = true;

			base.Open ();
		}
	}

	protected override void Close ()
	{
		base.Close ();

		//m_levelSections.SetActive (true);
		PlayerCar.m_popupOnScreen = false;
	}

	public void OnVehicleSelected (int index)
	{
		m_selected							= index;
		PlayerType		playerType			= (PlayerType) index;
		PlayerCar		player				= null;


		Color			disabledColor		= new Color32 (0, 0, 0, 15);

		for (int i = 0; i < m_playerVehicles.Length; i++) 
		{
			m_playerVehicles[i].SetActive (i == index);
			m_selectionGlows[i].enabled = i == index;

			if (i == index) 
			{
				player = m_playerVehicles [index].GetComponent<PlayerCar> ();
			}
		}

		if (player != null) 
		{
			Color buttonColor	= m_upgradeButtons[0].GetComponent<Image> ().color;
			buttonColor.a		= 1.0f;
			Color invalidColor	= new Color (buttonColor.r, buttonColor.g, buttonColor.b, 0.75f);

			for (int upgradeIndex = (int)UpgradeType.Tires; upgradeIndex <= (int)UpgradeType.Nitro; upgradeIndex++)
			{
				UpgradeType upgradeType = (UpgradeType)upgradeIndex;
				int buttonIndex = upgradeIndex;
				int stat 	= Mathf.RoundToInt (Mathf.Max (player.GetStat (upgradeType,true) * GameRules.k_pipCount, 1.0f));
				int stat2	= stat	+ player.GetUpgradeLevel (upgradeType);
				int statMax	= System.Math.Max( stat + player.GetMaxUpgradeLevel (upgradeType), GameRules.k_pipCount );

				m_upgradeButtons[buttonIndex].interactable					= player.CanUpgrade (upgradeType) && stat2 < GameRules.k_pipCount;
				m_upgradeButtons[buttonIndex].GetComponent<Image> ().color	= player.CanUpgrade (upgradeType) && stat2 < GameRules.k_pipCount ? buttonColor : invalidColor;

				SetPips (m_statPips[buttonIndex], stat);
				SetPips (m_upgradePips[buttonIndex], stat2);
				SetPips (m_maxUpgradePips[buttonIndex], statMax);
			}

			player.SetStartPosition (m_startX);
			EventRouter.Global.RouteEvent (GameEventCode.PlayerSelected, player);
		}

		switch (playerType) 
		{
			case PlayerType.Car:
			{
				m_specialAbilities[0].color = disabledColor;
				m_specialAbilities[1].color = disabledColor;
				break;
			}
			case PlayerType.Buggy:
			{
				m_specialAbilities[0].color = Color.white;
				m_specialAbilities[1].color = disabledColor;
				break;
			}
			case PlayerType.Tank:
			{
				m_specialAbilities[0].color = disabledColor;
				m_specialAbilities[1].color = Color.white;
				break;
			}
		}

		m_playButton.interactable = m_isAlive[index];
	}

	public void OnStatUpgraded (int index)
	{
		StartUpgrade (index);
	}

	public void OnPlayButton ()
	{
		if (m_isAlive[m_selected]) 
		{
			PlayerPrefs.SetInt (k_selectedCarPref, m_selected);
			PlayerPrefs.Save ();
			Close ();
			Time.timeScale = 1.0f;
			if (m_invincibleOnPlay) 
			{
				PlayerCar player = m_playerVehicles [m_selected].GetComponent<PlayerCar> ();
				if (player != null) 
				{
					player.SetInvincible ();
				}
				m_invincibleOnPlay = false;
			}
			EventRouter.Global.RouteEvent (UIScreenName.RaceHUD);
			EventRouter.Global.RouteEvent (GameEventCode.StartCountdown);
		}
	}

	public void OnBackButton ()
	{
		if (m_hasPlayed) 
		{
			EventRouter.Global.RouteEvent (UIEventCode.YesNoPopup, new YesNoPayload (OnExitLevel, "EXIT LEVEL"));
		} 
		else 
		{
			OnExitLevel ();
		}
	}

	private void OnExitLevel ()
	{
		for (int i = 0; i < m_playerVehicles.Length; i++) 
		{
			PlayerCar player = m_playerVehicles [i].GetComponent<PlayerCar> ();
			if (player != null) 
			{
				player.Reset ();
			}
		}

		Close ();
			m_levelSections.SetActive (false);
		EventRouter.Global.RouteEvent (UIScreenName.LevelSelect);
	}

	private void OnGamePaused ()
	{
		Open ();
		Time.timeScale	= 0.0f;
		m_hasPlayed		= true;
	}

	private void OnPlayerDied (System.Enum enumType, object parameter)
	{
		m_invincibleOnPlay	= true;
		m_startX			= (float) parameter;

		Invoke ("OnGamePaused", 1.0f);
	}

	private void SetPips (Image pipImage, int pipCount)
	{
		float x = pipImage.sprite.rect.width;
		pipImage.rectTransform.sizeDelta = new Vector2 (pipCount * x, pipImage.rectTransform.sizeDelta.y);
	}

	private void StartUpgrade (int index)
	{
		m_pendingUpgrade = index;

		PlayerCar player = m_playerVehicles [m_selected].GetComponent<PlayerCar> ();

		UpgradeType upgradeType = (UpgradeType) m_pendingUpgrade;

		int coinCost = 0;

		if (player != null) 
		{
			coinCost = GameRules.GetUpgradeCoinCost (player.GetUpgradeLevel (upgradeType), player.GetTotalUpgrades ());
		}

		if (coinCost > 0) 
		{
			YesNoPayload payload = new YesNoPayload (UpgradeConfirmed, null, "UPGRADE", upgradeType.ToString () + " +1\n" + coinCost.ToString () + " coins");
			EventRouter.Global.RouteEvent (UIEventCode.YesNoPopup, payload);
		} 
		else 
		{
			UpgradeConfirmed ();
		}
	}

	private void UpgradeConfirmed ()
	{
		PlayerCar player = m_playerVehicles [m_selected].GetComponent<PlayerCar> ();

		if (player != null) 
		{
			int coinCost = GameRules.GetUpgradeCoinCost (player.GetUpgradeLevel ((UpgradeType) m_pendingUpgrade), player.GetTotalUpgrades ());

			if (PlayerProfile.m_coinsCollected >= coinCost || coinCost <= 0) 
			{
				EventRouter.Global.RouteEvent (GameEventCode.CoinCollected, -coinCost);
				player.Upgrade ((UpgradeType)m_pendingUpgrade);
			} 
			else 
			{
				YesNoPayload payload = new YesNoPayload (null, null, "UPGRADE", "You need more coins!\n" + PlayerProfile.m_coinsCollected + " / " + coinCost, "OK", "CANCEL");
				EventRouter.Global.RouteEvent (UIEventCode.YesNoPopup, payload);
			}
		}

		OnVehicleSelected (m_selected);
	}
}
