﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UITopBar : UIScreen 
{
	const string		k_coinsPrefKey		= "CoinsCollected";

	[SerializeField]
	private Text[]		m_coinText			= null;

	private float		m_coinsCollected	= 0.0f;

	protected override void Start ()
	{
		base.Start ();

		PlayerProfile.m_coinsCollected	= PlayerPrefs.GetInt (k_coinsPrefKey, 0);
		m_coinsCollected				= (float) PlayerProfile.m_coinsCollected;

		RefreshCoinsText ();
	}

	void Update ()
	{
		if (m_coinsCollected != (float) PlayerProfile.m_coinsCollected) 
		{
			float diff = Mathf.Abs (m_coinsCollected - PlayerProfile.m_coinsCollected);

			float speed = 1.0f;

			if (diff > 1000.0f) 
			{
				speed = 150.0f;
			}
			else if (diff > 500.0f) 
			{
				speed = 50.0f;
			}
			else if (diff > 100.0f) 
			{
				speed = 10.0f;
			}


			m_coinsCollected = Mathf.Lerp (m_coinsCollected, (float) PlayerProfile.m_coinsCollected, Time.deltaTime * 2.0f / diff * 100.0f * speed);

			if (diff < 1.0f) 
			{
				m_coinsCollected = (float) PlayerProfile.m_coinsCollected;
			}
			RefreshCoinsText ();
		}
	}

	protected override void Subscribe ()
	{
		base.Subscribe ();

		SubscribeTo (GameEventCode.CoinCollected, OnCoinCollected);
	}

	private void OnCoinCollected (System.Enum enumType, object parameter)
	{
		int value = (int) parameter;
		PlayerProfile.m_coinsCollected += value;

		if (PlayerProfile.m_coinsCollected < 0) 
		{
			PlayerProfile.m_coinsCollected = 0;
		}

		RefreshCoinsText ();
		PlayerPrefs.SetInt (k_coinsPrefKey, PlayerProfile.m_coinsCollected);
		PlayerPrefs.Save ();
	}

	private void RefreshCoinsText ()
	{
		for (int i = 0; i < m_coinText.Length; i++) 
		{
			m_coinText[i].text = Mathf.RoundToInt (m_coinsCollected).ToString ();
		}	
	}
}
