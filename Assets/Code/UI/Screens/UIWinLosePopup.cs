﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIWinLosePopup : UIScreen 
{
	#region Serialized Data

	[SerializeField]
	private Text			m_winLoseText		= null;

	#endregion

	#region Event Subscriber Overrides

	protected override void Subscribe ()
	{
		base.Subscribe ();

		SubscribeTo (GameEventCode.Win, OnWin);
		SubscribeTo (GameEventCode.Lose, OnLose);
	}

	#endregion

	#region UI Callbacks

	public void OnOKButton ()
	{
		Close ();
		UnityEngine.SceneManagement.SceneManager.LoadScene (0);
	}

	#endregion

	#region Event Callbacks

	private void OnWin ()
	{
		m_winLoseText.text = "YOU WIN!";
		Open ();
	}

	private void OnLose ()
	{
		m_winLoseText.text = "GAME OVER!";
		Open ();
	}

	#endregion
}
