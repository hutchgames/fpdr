﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UITooltipPopup : UIScreen 
{
	[SerializeField]
	private Text				m_descriptionText		= null;

	[SerializeField]
	private RectTransform		m_pivot					= null;

	protected override void Subscribe ()
	{
		base.Subscribe ();

		SubscribeTo (UIEventCode.Tooltip, OnTooltip);
	}

	private void OnTooltip (System.Enum enumType, object parameter)
	{
		object[] parameterArray		= (object[]) parameter;
		m_pivot.position			= (Vector3) parameterArray[0];
		m_descriptionText.text		= (string) parameterArray[1];

		Open ();
	}

	void Update ()
	{
		if (!Input.GetMouseButton (0)) 
		{
			Close ();
		}
	}
}
