﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UILevelSelectPopup : UIScreen 
{
	[SerializeField]
	private string[]		m_levelPrefabs				= null;

	[SerializeField]
	private Transform		m_levelSpawnLocation		= null;

	private GameObject		m_lastLevel					= null;
	private int				m_lastLevelIndex			= -1;

	protected override void Start ()
	{
		base.Start ();
		//Open ();
	}

	protected override void Open ()
	{
		base.Open ();
		Time.timeScale = 1.0f;
	}

	public void OnQuitButton ()
	{
		EventRouter.Global.RouteEvent (UIEventCode.YesNoPopup, (object) new YesNoPayload (HelperMethods.Quit, "QUIT"));
	}

	public void OnLevelSelected (int levelIndex)
	{
		if (m_lastLevel != null) 
		{
			Destroy (m_lastLevel);
		}
		if (levelIndex < m_levelPrefabs.Length) 
		{
			Object prefab = Resources.Load (m_levelPrefabs[levelIndex]);
			if (prefab != null) 
			{
				GameObject obj = (GameObject) Instantiate (prefab, Vector3.zero, Quaternion.identity) as GameObject;
				if (obj != null) 
				{
					obj.transform.SetParent (m_levelSpawnLocation);
					m_lastLevel			= obj;
					m_lastLevelIndex	= levelIndex;
				}
			}
		}
		if (m_lastLevel != null) 
		{
			EventRouter.Global.RouteEvent (UIScreenName.VehicleSelect);
			Close ();
		}
	}
}
