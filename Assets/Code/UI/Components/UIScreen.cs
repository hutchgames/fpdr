﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIScreen : EventSubscriber 
{
	[SerializeField]
	protected UIScreenName m_name = UIScreenName.None;

	protected Canvas m_canvas = null;

	protected override void Subscribe ()
	{
		base.Subscribe ();

		if (m_name != UIScreenName.None) 
		{
			SubscribeTo (m_name, Open);
		}
	}

	protected virtual void Start ()
	{
		m_canvas = GetComponentInChildren<Canvas> ();
	}

	protected virtual void Open ()
	{
		m_canvas.enabled = true;
	}

	protected virtual void Close ()
	{
		m_canvas.enabled = false;
	}
}