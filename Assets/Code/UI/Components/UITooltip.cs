﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

[RequireComponent (typeof (EventTrigger))]
[RequireComponent (typeof (Graphic))]
public class UITooltip : MonoBehaviour 
{
	[SerializeField]
	private string				m_description			= null;

	[SerializeField]
	private Vector2				m_offset				= new Vector2 (0.0f, 50.0f);

	private Graphic				m_graphic				= null;
	private EventTrigger		m_eventTrigger			= null;
	private RectTransform		m_rectTransform			= null;

	void Start ()
	{
		m_graphic					= GetComponent<Graphic> ();
		m_eventTrigger				= GetComponent<EventTrigger> ();
		m_rectTransform				= GetComponent<RectTransform> ();
		m_graphic.raycastTarget		= true;
		m_eventTrigger.triggers		= new List<EventTrigger.Entry> ();
		EventTrigger.Entry entry	= new EventTrigger.Entry ();
		entry.eventID				= EventTriggerType.PointerDown;

		EventTrigger.TriggerEvent triggerEvent = new EventTrigger.TriggerEvent ();
		triggerEvent.AddListener (OnPointerDown);

		entry.callback				= triggerEvent;

		m_eventTrigger.triggers.Add (entry);
	}

	public void OnPointerDown (BaseEventData baseEventData)
	{
		if (enabled) 
		{
			Vector3 position = m_rectTransform.position;
			position.x += m_offset.x;
			position.y += m_offset.y;
			EventRouter.Global.RouteEvent (UIEventCode.Tooltip, new object[] { position, m_description }); 
		}
	}
}
