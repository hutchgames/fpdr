﻿using System;

public class YesNoPayload
{
	public Action m_yesCallback		= null;
	public Action m_noCallback		= null;
	public string m_title			= null;
	public string m_body			= null;
	public string m_yesString		= null;
	public string m_noString		= null;

	public YesNoPayload (Action yesCallback) 
	{
		m_yesCallback = yesCallback;
	}

	public YesNoPayload (Action yesCallback, string title) 
	{
		m_yesCallback	= yesCallback;
		m_title			= title;
	}

	public YesNoPayload (Action yesCallback, Action noCallback, string title, string body = null, string yesString = null, string noString = null) 
	{
		m_yesCallback	= yesCallback;
		m_noCallback	= noCallback;
		m_title			= title;
		m_body			= body;
		m_yesString		= yesString;
		m_noString		= noString;
	}
}