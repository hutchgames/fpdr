﻿public enum UIScreenName
{
	None = -1,
	VehicleSelect,
	LevelSelect,
	RaceHUD
}