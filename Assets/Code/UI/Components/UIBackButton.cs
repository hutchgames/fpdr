﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

[RequireComponent (typeof (Button))]
public class UIBackButton : MonoBehaviour 
{
	private Button m_button = null;

	public const float k_backButtonDeltaTime = 0.125f;

	public static float m_nextBackButton = 0.0f;

	void Start () 
	{
		m_button = GetComponent<Button> ();
	}

	void Update () 
	{
		if (Input.GetKeyDown (KeyCode.Escape) && Time.unscaledTime > m_nextBackButton) 
		{
			if (m_button.interactable && m_button.enabled && gameObject.activeInHierarchy)
			{
				
				List<RaycastResult>		results		= new List<RaycastResult> ();
				PointerEventData		data		= new PointerEventData (EventSystem.current);
				data.position						= m_button.transform.position;
				EventSystem.current.RaycastAll (data, results);

				if (results.Count > 0) 
				{
					if (results[0].gameObject == m_button.gameObject) 
					{
						m_nextBackButton = Time.unscaledTime + k_backButtonDeltaTime;
						m_button.onClick.Invoke ();
					}
				}
			}
		}
	}
}
