﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

[ExecuteInEditMode]
[RequireComponent (typeof (Button))]
[RequireComponent (typeof (Image))]
public class UIBackButtonNonClickable : MonoBehaviour 
{
	private Button	m_button	= null;
	private Image	m_image		= null;

	void Start () 
	{
		m_button			= GetComponent<Button> ();
		m_image				= GetComponent<Image> ();
		m_button.enabled	= false;
		m_image.enabled		= false;
	}

	void Update () 
	{
		if (Application.isPlaying)
		{
			if (Input.GetKeyDown (KeyCode.Escape) && Time.unscaledTime > UIBackButton.m_nextBackButton) 
			{
				if (m_button.interactable && gameObject.activeInHierarchy)
				{
					m_button.enabled					= true;
					m_image.enabled						= true;

					List<RaycastResult>		results		= new List<RaycastResult> ();
					PointerEventData		data		= new PointerEventData (EventSystem.current);
					data.position						= m_button.transform.position;
					EventSystem.current.RaycastAll (data, results);

					if (results.Count > 0) 
					{
						if (results [0].gameObject == m_button.gameObject) 
						{
							UIBackButton.m_nextBackButton = Time.unscaledTime + UIBackButton.k_backButtonDeltaTime;
							m_button.onClick.Invoke ();
						}
					}
				}
			}
		}

		if (m_button.enabled) 
		{
			m_button.enabled = false;
		}

		if (m_image.enabled) 
		{
			m_image.enabled = false;
		}
	}
}
