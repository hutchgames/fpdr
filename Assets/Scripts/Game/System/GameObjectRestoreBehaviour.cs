//////////////////////////////////////////////////////////////////////////
/// @file	RestoreBehaviour.cs
///
/// @author	Steve Jopling (SJ)
///
/// @brief	Interface for restoring a game object to it's original state.
///
/// @note 	This script should be applied to any game object that you want to restore.
//////////////////////////////////////////////////////////////////////////

/************************ EXTERNAL NAMESPACES ***************************/

using UnityEngine;																// Unity 			(ref http://docs.unity3d.com/Documentation/ScriptReference/index.html)
using System;																	// String / Math 	(ref http://msdn.microsoft.com/en-us/library/system.aspx)
using System.Collections;														// Queue 			(ref http://msdn.microsoft.com/en-us/library/system.collections.aspx)
using System.Collections.Generic;												// List<> 			(ref http://msdn.microsoft.com/en-us/library/system.collections.generic.aspx)

/************************** THE SCRIPT CLASS ****************************/

//////////////////////////////////////////////////////////////////////////
/// @brief	RestoreBehaviour class.
//////////////////////////////////////////////////////////////////////////
public class GameObjectRestoreBehaviour : RestoreBehaviour
{

/***************************** PRIVATE DATA *****************************/

private Transform							m_transform;
private Rigidbody							m_rigidbody;
private Collider							m_collider;
private Renderer							m_renderer;
	
// NOTE: Add additional serialized fields as and when required by the components that you want to restore.

[SerializeField] private Vector3			m_initialTransformPosition;
[SerializeField] private Quaternion			m_initialTransformRotation;
[SerializeField] private bool				m_initialRigidbodyIsKinematic;
[SerializeField] private bool				m_initialColliderEnabled;
[SerializeField] private bool				m_initialRendererEnabled;

[SerializeField] private RestoreBehaviour[]	m_restoreBehaviours;

/*************************** PUBLIC ACCESSORS ***************************/

public Vector3 RestorePosition
{
	set { m_initialTransformPosition = value; }
	get { return m_initialTransformPosition; }
}

public Quaternion RestoreRotation
{
	set { m_initialTransformRotation = value; }
	get { return m_initialTransformRotation; }
}

/************************* PUBLIC STATIC METHODS ************************/

public static void SaveObjectState(GameObject obj)
{
	Transform parentTransform = obj.transform;
	
	// Save the initial state of the parent "restorable" gameobject.
	GameObjectRestoreBehaviour parentGameObjectRestoreBehaviour = obj.GetComponent<GameObjectRestoreBehaviour>();
	if (parentGameObjectRestoreBehaviour != null)
	{
		// Save the initial state of any child "restorable" gameobjects.
		int nNumChildren = parentTransform.childCount;
		for (int j = 0; j < nNumChildren; ++j)
		{
			Transform childTransform = parentTransform.GetChild(j);
			GameObjectRestoreBehaviour childGameObjectRestoreBehaviour = childTransform.GetComponent<GameObjectRestoreBehaviour>();
			if (childGameObjectRestoreBehaviour != null)
			{
				// Save the initial state of any grandchildren "restorable" gameobjects.
				int nNumGrandChildren = childTransform.childCount;
				for (int k = 0; k < nNumGrandChildren; ++k)
				{
					Transform grandChildTransform = childTransform.GetChild(k);
					GameObjectRestoreBehaviour grandChildGameObjectRestoreBehaviour = grandChildTransform.GetComponent<GameObjectRestoreBehaviour>();
					if (grandChildGameObjectRestoreBehaviour != null)
						grandChildGameObjectRestoreBehaviour.SaveState();
				}
					
				childGameObjectRestoreBehaviour.SaveState();
			}
		}
		
		parentGameObjectRestoreBehaviour.SaveState();
	}
}

public static void RestoreObjectState(GameObject obj)
{
	GameObjectRestoreBehaviour[] oGameObjectRestoreBehaviours = obj.GetComponentsInChildren<GameObjectRestoreBehaviour>();
	if (oGameObjectRestoreBehaviours != null)
	{
		int nNumGameObjectRestoreBehaviours = oGameObjectRestoreBehaviours.Length;
		for (int i = 0; i < nNumGameObjectRestoreBehaviours; ++i)
			oGameObjectRestoreBehaviours[i].RestoreState();
	}
}

/************************* PUBLIC CLASS METHODS *************************/

//////////////////////////////////////////////////////////////////////////
/// @brief Initialise class after construction.
//////////////////////////////////////////////////////////////////////////
void Awake()
{
	// Cache required components.
	m_transform = transform;
	m_rigidbody = GetComponent<Rigidbody>();
	m_collider = GetComponent<Collider>();
	m_renderer = GetComponent<Renderer>();
}

//////////////////////////////////////////////////////////////////////////
/// @brief	Save the initial state.
/// 		NOTE: May get called before Awake(), hence not using cached components.
//////////////////////////////////////////////////////////////////////////
public override void SaveState()
{
	m_initialTransformPosition = transform.localPosition;
	m_initialTransformRotation = transform.localRotation;
		
	if (GetComponent<Rigidbody>() != null)
		m_initialRigidbodyIsKinematic = GetComponent<Rigidbody>().isKinematic;
	
	if (GetComponent<Collider>() != null)
		m_initialColliderEnabled = GetComponent<Collider>().enabled;
	
	if (GetComponent<Renderer>() != null)
		m_initialRendererEnabled = GetComponent<Renderer>().enabled;
		
	// Cache all restore components at this hierarchy level.
	RestoreBehaviour[] restoreBehaviours = GetComponents<RestoreBehaviour>() as RestoreBehaviour[];
	if (restoreBehaviours != null)
	{
		int nNumRestoreBehaviours = restoreBehaviours.Length;
		if (nNumRestoreBehaviours > 1)
		{
			int nRestoreBehaviourIndex = 0;
			
			// Don't include this GameObjectRestoreBehaviour in the array.
			m_restoreBehaviours = new RestoreBehaviour[nNumRestoreBehaviours - 1];
			
			for (int i = 0; i < nNumRestoreBehaviours; ++i)
			{
				if (restoreBehaviours[i] != this)
				{
					m_restoreBehaviours[nRestoreBehaviourIndex] = restoreBehaviours[i];
					m_restoreBehaviours[nRestoreBehaviourIndex].SaveState();
					nRestoreBehaviourIndex++;
				}
			}
		}
	}
}

//////////////////////////////////////////////////////////////////////////
/// @brief	Restore the saved initial state.
/// 		NOTE: Will get called after Awake(), so using cached components.
//////////////////////////////////////////////////////////////////////////
public override void RestoreState()
{
	m_transform.localPosition = m_initialTransformPosition;
	m_transform.localRotation = m_initialTransformRotation;
	
	if (m_rigidbody != null)
	{
		m_rigidbody.isKinematic = m_initialRigidbodyIsKinematic;
		
		if (!m_initialRigidbodyIsKinematic)
		{
			m_rigidbody.velocity = Vector3.zero;
			m_rigidbody.angularVelocity = Vector3.zero;
		}
	}
	
	if (m_collider != null)
		m_collider.enabled = m_initialColliderEnabled;
	
	if (m_renderer != null)
		m_renderer.enabled = m_initialRendererEnabled;
	
	// Propagate to all restore components at this hierarchy level.
	if (m_restoreBehaviours != null)
	{
		int nNumRestoreBehaviours = m_restoreBehaviours.Length;
		for (int i = 0; i < nNumRestoreBehaviours; ++i)
			m_restoreBehaviours[i].RestoreState();
	}
}
	
}
