//////////////////////////////////////////////////////////////////////////
/// @file	RestoreBehaviour.cs
///
/// @author	Steve Jopling (SJ)
///
/// @brief	Interface for restoring an object to it's original state.
///
/// @note 	This script should be applied to any component that you want to restore.
//////////////////////////////////////////////////////////////////////////

/************************ EXTERNAL NAMESPACES ***************************/

using UnityEngine;																// Unity 			(ref http://docs.unity3d.com/Documentation/ScriptReference/index.html)

/************************** THE SCRIPT CLASS ****************************/

//////////////////////////////////////////////////////////////////////////
/// @brief	RestoreBehaviour class.
//////////////////////////////////////////////////////////////////////////
public abstract class RestoreBehaviour : EventSubscriber
{

/************************* PUBLIC CLASS METHODS *************************/

	protected override void Subscribe ()
	{
		base.Subscribe ();

		SubscribeTo (GameEventCode.Reset, RestoreState);
	}

//////////////////////////////////////////////////////////////////////////
/// @brief	Override to save the initial state.
//////////////////////////////////////////////////////////////////////////
public abstract void SaveState();

//////////////////////////////////////////////////////////////////////////
/// @brief	Called to restore the initial state.
//////////////////////////////////////////////////////////////////////////
public abstract void RestoreState();
	
}
