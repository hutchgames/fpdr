using UnityEngine;
using System.Collections;
using System.Collections.Generic;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class PropDamageController : RestoreBehaviour
{

	public int m_nStrength = 0;							// JL: prop break strength, higher = stronger
	public int m_nPointsValue = 25;						// Points you earn for breaking this prop
	
	public bool m_bOverrideCenterOfMass = false;
	public Vector3 m_vCenterOfMass = Vector3.zero;
	public bool m_bFixedUntilBroken = false;
	public float m_fBreakForce = 1000f;
	public bool m_bDisableColliderOnBreak = false;
	public bool m_bDisableRendererOnBreak = false;
	public GameObject[] m_pParticlesToPlay;
	public Vector3 m_vParticleOffset;
	public bool m_bOrientToCollision = true;
	public bool m_bDontDelete = false;
	public bool m_bRotateOnCollision = false;
	public bool m_bOrientObjectToCollision = false;
	public Vector3 m_vRotateAngles;
	public bool m_bApplyForceToDamagedBits = false;
	public bool m_bApplyForceInAllDirections = false;
	public float m_fForceScale = 15f;

	private List<Transform> m_tMasterObjects;
	private List<Transform> m_tDamagedObjects;
	
	private bool m_bHaveParticles = false;
	private int[] m_nParticlePoolHashCodes;
	
	private Transform m_transform;
	private Renderer m_renderer;
	private Rigidbody m_rigidbody;
	private Collider m_collider;
	private bool m_bBroken = false;
	private bool m_bMoved;
	private bool m_bVisible;
	private FixedJoint m_fixedJoint;
	
	private bool m_bUnbreakable;
	private bool m_bTemporarilyWeakened;
	private bool m_bIsTrigger;
	private float m_fScaledMass;
	

	// Restore state.
	[SerializeField] private Vector3 m_initialRigidbodyCenterOfMass;
	[SerializeField] private float m_fInitialRigidbodyMass;
	[SerializeField] private bool m_bInitialRigidbodyIsKinematic;
	
	public int Strength
	{
		get { return m_nStrength; }
	}
	
	public bool Breakable
	{
		get { return !m_bBroken && (!m_bUnbreakable || m_bTemporarilyWeakened); }
	}
	
	public bool Unbreakable
	{
		get { return m_bUnbreakable; }
	}
	
	public bool TemporarilyWeakend
	{
		get { return m_bTemporarilyWeakened; }
	}
	
	public bool Visible
	{
		get { return m_bVisible; }
	}
	

	
	public override void SaveState()
	{
		if (GetComponent<Rigidbody>() != null)
		{
			m_initialRigidbodyCenterOfMass = GetComponent<Rigidbody>().centerOfMass;
			m_fInitialRigidbodyMass = GetComponent<Rigidbody>().mass;
			m_bInitialRigidbodyIsKinematic = GetComponent<Rigidbody>().isKinematic;
		}
	}
	
	public override void RestoreState()
	{
		m_bBroken = false;
		m_bMoved = false;
		m_bVisible = false;
		m_bUnbreakable = false;
		m_bTemporarilyWeakened = false;
		
		// Restore the fixed joint?
		if (m_bFixedUntilBroken)
			AddFixedJoint();
		
		if (m_tMasterObjects != null)
		{
			int nNumMasterObjects = m_tMasterObjects.Count;
			for (int i = 0; i < nNumMasterObjects; ++i)
			{
				Transform tMasterObject = m_tMasterObjects[i];
				if (tMasterObject != null)
					tMasterObject.gameObject.SetActive(true);
#if UNITY_EDITOR
				else
					Debug.LogWarning(gameObject.name +  ": missing master object: " + i);
#endif // UNITY_EDITOR
			}
		}
		
		if (m_tDamagedObjects != null)
		{
			int nNumDamagedObjects = m_tDamagedObjects.Count;
			for (int i = 0; i < nNumDamagedObjects; ++i)
			{
				Transform tDamagedObject = m_tDamagedObjects[i];
				if (tDamagedObject != null)
					tDamagedObject.gameObject.SetActive(false);
#if UNITY_EDITOR
				else
					Debug.LogWarning(gameObject.name +  ": missing damage object: " + i);
#endif // UNITY_EDITOR
			}
		}
		
		if (m_rigidbody != null)
		{
			m_rigidbody.centerOfMass = m_initialRigidbodyCenterOfMass;
			m_rigidbody.mass = m_fInitialRigidbodyMass;
			m_rigidbody.isKinematic = m_bInitialRigidbodyIsKinematic;
		}
	}
	
	public void Awake()
	{
		base.Awake();

		// JL: we manually call awake on pooled props, so ensure Unity's automatic call is ignored
		if (m_transform != null)
			return;
	
		m_bDontDelete = true;	// SJ - Don't delete broken props out of range of the camera for now.
		
		// Cache components
		m_renderer = GetComponent<Renderer>();
		m_transform = transform;
		m_rigidbody = GetComponent<Rigidbody>();
		m_collider = GetComponent<Collider>();
		
		m_tMasterObjects = new List<Transform>();
		m_tDamagedObjects = new List<Transform>();
		
		// Assign any children with master or damaged state tags to corresponding list
		int nChildCount = m_transform.childCount;
		for (int i = 0; i < nChildCount; ++i)
		{
			Transform tChildTransform = m_transform.GetChild(i);
			
			PropDamageStateMaster propDamageStateMaster = tChildTransform.GetComponent<PropDamageStateMaster>();
			if (propDamageStateMaster != null)
			{
				if (propDamageStateMaster.HideWhenDamaged)
					m_tMasterObjects.Add(tChildTransform);
				
				// Make sure master objects start active.
				tChildTransform.gameObject.SetActive(true);
			}
			else
			{
				PropDamageStateDamaged propDamageStateDamaged = tChildTransform.GetComponent<PropDamageStateDamaged>();
				if (propDamageStateDamaged != null)
				{
					if (propDamageStateDamaged.UnhideWhenDamaged)
					{
						// Make sure we are going to show this debris piece at all.
						m_tDamagedObjects.Add(tChildTransform);
					}
					
					// Deactivate damaged objects until damaged
					tChildTransform.gameObject.SetActive(false);
				}
			}
		}
		
		// Set initial centre of mass?
		if (m_bOverrideCenterOfMass && m_rigidbody)
			m_rigidbody.centerOfMass = m_vCenterOfMass;
	}
	
	void Start()
	{
		// JL: moved to start, as we now move the prop after creation
		if (m_bFixedUntilBroken)
			AddFixedJoint();
		
		
		m_bMoved = false;
		m_bVisible = false;
	}
	



	
	void AddFixedJoint()
	{
		if (m_fixedJoint != null)	// Don't allow multiple fixed joints.
			return;
		
		m_fixedJoint = gameObject.AddComponent<FixedJoint>();
		if (m_fixedJoint != null)
		{
			m_fixedJoint.breakForce = m_fBreakForce;
			m_fixedJoint.breakTorque = (m_fBreakForce * 2);
		}
	}
	
	void OnJointBreak()
	{
		// this shouldn't happen if unbreakable, so don't have to do anything here
		Vector3 vDirection = Vector3.forward;
		
		if (m_rigidbody != null)
			vDirection = m_rigidbody.velocity;
				
		ProcessHit(vDirection,vDirection);
	}
	
	void OnTriggerEnter(Collider coll)
	{
		// TODO ideally we should deactivate the trigger so we don't get called at all!
		if (m_bBroken)
			return;
	
		GameObject colliderGameObject = coll.gameObject;
	


		


		
		if (this.Breakable)
		{
			Vector3 vDirection = Vector3.forward;
			
			if (coll.GetComponent<Rigidbody>() != null)
				vDirection = coll.GetComponent<Rigidbody>().velocity;
			
			// Add rotation angles?
			if (m_bRotateOnCollision)
			{
				if (m_bOrientObjectToCollision)
				{
					// Flatten collision vector
					Vector3 vOrientVector = vDirection;
					vOrientVector.y = 0;
					vOrientVector = m_transform.position + vOrientVector;
					
					// Aim object at collision vector
					m_transform.LookAt(vOrientVector);
				}
				
				if (m_vRotateAngles.sqrMagnitude >= 0.0f)
					m_transform.localRotation = Quaternion.Euler(m_transform.localEulerAngles + m_vRotateAngles);
			}
			
			ProcessHit(vDirection, vDirection);

		}
	}
	

	
	void OnCollisionEnter(Collision collision)
	{
		if (m_bOverrideCenterOfMass && m_rigidbody)
			m_rigidbody.centerOfMass = m_vCenterOfMass;
		
		GameObject colliderGameObject = collision.collider.gameObject;

		
		Rigidbody body = collision.collider.attachedRigidbody;
		if (body != null && m_rigidbody != null && !m_rigidbody.isKinematic)
			m_bMoved = true;
		
		if (!m_bFixedUntilBroken && !m_bBroken && body && this.Breakable)
		{
			float fForce = Vector3.Dot(collision.contacts[0].normal, collision.relativeVelocity) * body.mass;
			
			if (fForce > m_fBreakForce)
			{
				Vector3 vDirection = Vector3.forward;
				
				if (m_rigidbody != null)
					vDirection = m_rigidbody.velocity;
				
				ProcessHit(collision.rigidbody.velocity, vDirection);
			}
		}
	}
	
	void ProcessHit(Vector3 vForceDirection, Vector3 vParticleDirection)
	{
		SwapToDamagedObjects(vForceDirection);
		
		PlayParticles(vParticleDirection);

		m_bBroken = true;
		
		if (m_bDisableColliderOnBreak)
		{
			m_collider.enabled = false;
			if (m_rigidbody != null)
				m_rigidbody.isKinematic = true;
		}
		
		if (m_bDisableRendererOnBreak && m_renderer)
			m_renderer.enabled = false;
	
	}
	
	void PlayParticles(Vector3 vDirection)
	{
		//Play all particles
		if (m_bHaveParticles)
		{
			Quaternion qRotation = m_transform.rotation;
			
			if (m_bOrientToCollision)
				qRotation = Quaternion.LookRotation(vDirection);

			for (int i = 0; i < m_pParticlesToPlay.Length; ++i)	
			{
				// AETODO:
				//m_pParticlesToPlay
			}
			
//			int nNumParticlePoolHashCodes = m_nParticlePoolHashCodes.Length;
//			for (int i = 0; i < nNumParticlePoolHashCodes; ++i)	
//				ParticleSystemCache.Get.Play(m_nParticlePoolHashCodes[i],m_transform,m_vParticleOffset,qRotation);	
		}
	}
	
	void SwapToDamagedObjects(Vector3 vForceVector)
	{
		//Turn off master objects
		if (m_tMasterObjects != null)
		{
			int nNumMasterObjects = m_tMasterObjects.Count;
			for(int i = 0; i < nNumMasterObjects; i++)
			{
				Transform tMasterObject = m_tMasterObjects[i];
				if (tMasterObject != null)
					tMasterObject.gameObject.SetActive(false);
#if UNITY_EDITOR
				else
					Debug.LogWarning(gameObject.name +  ": missing master object: " + i);
#endif // UNITY_EDITOR
			}
		}
		
		//Turn on damaged objects
		if (m_tDamagedObjects != null)
		{
			int nNumDamagedObjects = m_tDamagedObjects.Count;
			for(int i = 0; i < nNumDamagedObjects; i++)
			{
				Transform tDamagedObject = m_tDamagedObjects[i];
				if (tDamagedObject != null)
					tDamagedObject.gameObject.SetActive(true);
#if UNITY_EDITOR
				else
					Debug.LogWarning(gameObject.name +  ": missing damage object: " + i);
#endif // UNITY_EDITOR
			
				// Add force to damaged pieces if required
				Rigidbody damagedRigidbody = tDamagedObject.GetComponent<Rigidbody>();
				Vector3 damagedPosition = tDamagedObject.position;
				if (m_bApplyForceToDamagedBits && damagedRigidbody)
				{
					if (!m_bApplyForceInAllDirections)
						damagedRigidbody.AddForceAtPosition(vForceVector * damagedRigidbody.mass * m_fForceScale, damagedPosition + new Vector3(0, 1, 0));
					else
						damagedRigidbody.AddForceAtPosition((damagedPosition - m_transform.position) * damagedRigidbody.mass * m_fForceScale, damagedPosition + vForceVector.normalized);
				}
			}
		}
	}
	

	

	public bool Broken
	{
		get { return m_bBroken; }
	}
	
	public bool Moved
	{
		get { return m_bMoved; }
	}
}