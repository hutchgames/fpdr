using UnityEngine;
using System.Collections;

public class PropDamageStateDamaged : MonoBehaviour
{
	public bool m_bUnhideWhenDamaged = true;
	public bool m_bUnhideOnLowPowerDevices = false;
	
	public bool UnhideWhenDamaged
	{
		get { return m_bUnhideWhenDamaged; }
	}
	
	public bool UnhideOnLowPowerDevices
	{
		get { return m_bUnhideOnLowPowerDevices; }
	}
}
